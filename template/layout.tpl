<!DOCTYPE html>
<html lang="fr">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Gestion de projet">
        <meta name="author" content="Jérôme Dumas">

        <title>Karotte.fr - Organisation de projets</title>

        <!-- Bootstrap Core CSS -->
        <link href="{$live_url}/template/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="{$live_url}/template/css/styles.css" rel="stylesheet">

        <link href="{$live_url}/template/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        {section name="itemCss" loop=$css}
            <link rel="stylesheet" type="text/css" href="{$live_url}/template/css/{$css[itemCss]}.css" />
        {/section}     

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>

    <body>

        <!-- POPUP pour envoi multiple de fichiers. Obligatoire de le placer ici -->
        <div class="modal fade" id="modalDialog" tabindex="-1" role="dialog" aria-labelledby="modalDialogLabel"></div>
        <!-- Fin de la popup -->        

        <div id="wrapper">

            <!-- Sidebar -->
            <div id="sidebar-wrapper">
                <ul class="sidebar-nav">                     
                    {foreach $main_menu as $key => $menu}

                        {foreach $menu as $key => $link}
                            {if $key != "icon" && $key != "target"}
                                {if $link|@count > 1}
                                    <li>
                                        <a data-target="#{$key}" data-toggle="collapse" href="javascript:void(0);" aria-expanded="true"><i class="{$menu.icon} fa fa-2x"></i> <i class="fa fa-fw fa-caret-down"></i></a>
                                        <ul class="collapse in" id="{$key}" aria-expanded="true">
                                            {foreach $link as $key => $value}
                                                <li class="{if $breadcrumb[0].url == $key} active {/if}"><a href="{$value}">{$key}</a></li>
                                                {/foreach}
                                        </ul>
                                    </li> 
                                {else}
                                    {if $menu.target == "href"}
                                        <li class="{if $breadcrumb[0].url == $key} active {/if}"><a href="{$link}"><i class="{$menu.icon} fa-2x"  data-toggle="tooltip" data-delay="400" data-placement="right" title="{$key}"></i></a></li>
                                            {else}
                                        <li class="{if $breadcrumb[0].url == $key} active {/if}"><a href="javascript:void(0);" class="ajaxSubmenu" data-ajax-url="{$link}"><i class="{$menu.icon} fa-2x"  data-toggle="tooltip" data-delay="400" data-placement="right" title="{$key}"></i></a></li>
                                            {/if}
                                        {/if}
                                    {/if}
                                {/foreach}
                            {/foreach}

                    <li class="logout"><a href="{$live_url}/modules/users/index.php?page=executeLogout" data-toggle="tooltip" data-delay="400" data-placement="right" title="Deconnexion"><i class="fa fa-lock fa-2x" ></i></a></li>
                </ul>
            </div>

            <div id="sub-sidebar" class="sub-sidebar sub-sidebar-hidden">
                <div id="content-sub-sidebar"></div>
            </div>
            <!-- /#sidebar-wrapper -->

            <!-- Page Content -->
            <ol class="breadcrumb"><i class="fa fa-code-fork"></i>
                {section name="item" loop=$breadcrumb}
                    <li><a href="{$breadcrumb[item].url}">{$breadcrumb[item].label}</a></li>
                {/section}
                {if $actions}
                    {section name="item" loop=$actions[$view]["actions"]}
                    <li style="float:right;"><a href="{$actions[$view]["actions"][item].href}" data-ajax="{$actions[$view]["actions"][item].ajax}" class="{$actions[$view]["actions"][item].class}">{$actions[$view]["actions"][item].html}</a></li>
                    {/section}
                {/if}
            </ol>             
            <div id="page-content-wrapper" class="remove-sub-sidebar">

                <div class="container-fluid">
                    <div class='col-lg-12 col-md-12 no-padding'>
                        {$content}
                    </div>
                </div>
            </div>
            <!-- /#page-content-wrapper -->

        </div>
        <!-- /#wrapper -->

        <!-- jQuery -->
        <script src="{$live_url}/template/js/jquery.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="{$live_url}/template/js/bootstrap.min.js"></script>
        <script src="{$live_url}/template/js/sidebars.js"></script>

        <!-- Morris Charts JavaScript -->
        <!-- <script src="{$live_url}/template/js/plugins/morris/raphael.min.js"></script>
        <script src="{$live_url}/template/js/plugins/morris/morris.min.js"></script>
        <script src="{$live_url}/template/js/plugins/morris/morris-data.js"></script> -->

        {section name="item" loop=$javascript}
            <script type="text/javascript" src="{$live_url}/template/js/{$javascript[item]}.js"></script>
        {/section}

        {if $alert}
            <script type="text/javascript" src="{$live_url}/template/js/plugins/notify.min.js"></script>
            <script type="text/javascript">
                $.notify("{$alert}");
            </script>
        {/if}

    </body>

</html>