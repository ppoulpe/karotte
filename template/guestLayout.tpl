<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="description" content="" />
        <meta name="author" content="" />

        <title>SB Admin - Bootstrap Admin Template</title>

        <!-- Bootstrap Core CSS -->
        <link href="{$live_url}/template/css/bootstrap.min.css" rel="stylesheet" />
        <link href="{$live_url}/template/css/login.css" rel="stylesheet" />

    </head>

    <body>
        <div class="container">
            <div class="login-container">
                <div id="output"></div>
                <div class="avatar"></div>
                <div class="form-box">
                    <form action="index.php?page=executeLogin" method="post">
                        <input type="text" class="form-control" id="username" name="username" placeholder="Identifiant"/>
                        <input type="password" class="form-control" id="password" name="password" placeholder="Mot de passe"/>
                        <button type="submit" class="btn btn-info btn-block login">Connexion</button>
                    </form>            
                </div>
            </div>        
        </div>    

    </body>
</html>
