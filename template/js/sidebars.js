/* 
 * Copyright (C) 2015 Jérôme Dumas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

function toggleSidebar(content) {
    if ($(".sub-sidebar").hasClass("sub-sidebar-hidden")) { // Il est masqué, donc je l'affiche

        $("#content-sub-sidebar").html(content);

        // Sous menu
        $(".sub-sidebar").removeClass("sub-sidebar-hidden");
        $(".sub-sidebar").addClass("sub-sidebar-visible");

        // Contenu décallage
        $("#page-content-wrapper").addClass("add-sub-sidebar");
        $("#page-content-wrapper").removeClass("remove-sub-sidebar");

    } else { // il est affiché, donc je le masque

        $("#content-sub-sidebar").html();

        // Sous menu
        $(".sub-sidebar").addClass("sub-sidebar-hidden");
        $(".sub-sidebar").removeClass("sub-sidebar-visible");

        $("#page-content-wrapper").removeClass("add-sub-sidebar");
        $("#page-content-wrapper").addClass("remove-sub-sidebar");
        //toggleSidebar(content);
    }
}

function openSidebar(content) {
    $("#content-sub-sidebar").html(content);

    // Sous menu
    $(".sub-sidebar").removeClass("sub-sidebar-hidden");
    $(".sub-sidebar").addClass("sub-sidebar-visible");

    // Contenu décallage
    $("#page-content-wrapper").addClass("add-sub-sidebar");
    $("#page-content-wrapper").removeClass("remove-sub-sidebar");
    $(".breadcrumb").addClass("add-sub-sidebar-breadcrumb");

}

function closeSidebar() {

    $("#content-sub-sidebar").html();

    // Sous menu
    $(".sub-sidebar").addClass("sub-sidebar-hidden");
    $(".sub-sidebar").removeClass("sub-sidebar-visible");

    $("#page-content-wrapper").removeClass("add-sub-sidebar");
    $("#page-content-wrapper").addClass("remove-sub-sidebar");
    $(".breadcrumb").removeClass("add-sub-sidebar-breadcrumb");
}

