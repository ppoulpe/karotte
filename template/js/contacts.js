/* 
 * Copyright (C) 2016 Jérôme Dumas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

$(function(){
    
    $(document).on("click","button.buttonAddContact",function(){
        
        $.ajax({
            type:"POST",
            url: "index.php?page=executeAddContact",
            data: {
                name : $("input#name").val(),
                surname : $("input#surname").val(),
                email : $("input#email").val(),
                phone : $("input#phone").val(),
                cellphone : $("input#cellphone").val(),
                address : $("input#address").val(),
                address_2 : $("input#address_2").val(),
                zip : $("input#zip").val(),
                city : $("input#city").val(),
                notes : $("textarea#notes").val()
            },
            success: function (responseHTML) {
                $("#modalDialog").modal('toggle');
            }
        });        
    });   
    
    $(document).on("click","button.buttonEditContact",function(){
        
        $.ajax({
            type:"POST",
            url: "index.php?page=executeUpdateContact",
            data: {
                name : $("input#name").val(),
                surname : $("input#surname").val(),
                email : $("input#email").val(),
                phone : $("input#phone").val(),
                cellphone : $("input#cellphone").val(),
                address : $("input#address").val(),
                address_2 : $("input#address_2").val(),
                zip : $("input#zip").val(),
                city : $("input#city").val(),
                notes : $("textarea#notes").val(),
                contacts_id : $("input#contacts_id").val()
            },
            success: function (responseHTML) {
                
                // Mise à jour de la page du contact
                $.ajax({
                    type:"POST",
                    url: "index.php?page=executeFind",
                    data: {
                        cid : $("input#contacts_id").val()
                    },
                    success: function (responseHTML) {
                        $(".file-content-container").html(responseHTML);
                    }
                });                 
                
                $("#modalDialog").modal('toggle');
            }
        });        
    });   
    
    $(document).on("click","a.deleteContact",function(){
        if(confirm("Êtes-vous sûr de vouloir supprimer ce contact ?")){
            $.ajax({
                type:"POST",
                url: "index.php?page=executeDeleteContact",
                data: {
                    cid : $(this).attr("id")
                },
                success: function () {
                    window.location = "index.php"
                }
            });             
        }
    });
    
    $(document).on("click","a.find",function(){
        
        $.ajax({
            type:"POST",
            url: "index.php?page=executeFind",
            data: {
                cid : $(this).attr("id")
            },
            success: function (responseHTML) {
                $(".file-content-container").html(responseHTML);
            }
        });        
    });     
});