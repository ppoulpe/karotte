/* 
 * Copyright (C) 2015 Jérôme Dumas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

$(function () {

    $(document).on("click", "a.modalDialog", function () {

        $.ajax({
            url: $(this).attr("data-ajax"),
            data: {
                title: $(this).attr("title"),
                dialog: $(this).attr("id"),
                currentFolder: $(".file-content").attr("data-folder")
            },
            success: function (responseHTML) {
                $("#modalDialog").html(responseHTML);
                $("#modalDialog").modal('show');
            }
        });


    });

    $(document).on("click", "a.ajaxSubmenu", function () {

        var obj = $(this);

        // On test sir la class est active.
        if (!obj.hasClass("active") || obj.hasClass("persist")) {

            $.ajax({
                url: $(this).attr("data-ajax-url"),
                success: function (responseHTML) {

                    closeSidebar();

                    setTimeout(function () {
                        openSidebar(responseHTML);
                    }, 500);

                    $(".sidebar-nav a").removeClass("active");
                    $(".sidebar-nav a").removeClass("active");

                    $(".sidebar-nav a").removeClass("active_load");
                    $("#sidebar-wrapper").addClass("shadowed");
                    obj.addClass("active");

                }
            });

        } else {
            $("#sidebar-wrapper").removeClass("shadowed");
            obj.removeClass("active");
            closeSidebar();
        }

    });

    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
    
    /**
     * @desc ensemble de fonctions qui permettent le tri des listes dans la sidebar
     * @returns {undefined}
     */
    $(function(){
       $(document).on("click","a.firstLetter",function(){
           $(".sidebar").css("overflow","hidden");
           $(".overlay").css("top",$(".sidebar").scrollTop());
           $(".overlay").fadeIn();
       }); 
       
       $(document).on("click","a.filterByFirstLetter",function(){
           $(".sidebar").css("overflow","auto");
           $(".overlay").fadeOut();
       });        
       
       $(document).on("click","a.closefilter",function(){
           $(".sidebar").css("overflow","auto");
           $(".overlay").fadeOut();
       });
       
    });
});

