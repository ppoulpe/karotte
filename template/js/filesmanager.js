/* 
 * Copyright (C) 2015 Jérôme Dumas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

$(function () {
    
    if ($(".file-tree")) {


        var fileFilename = $("div.file-content").attr("data-folder");

        $.ajax({
            url: "index.php",
            data: {
                page: "executeGetFileContent",
                filename: fileFilename
            },
            success: function (responseString) {

                //$('.file-tree').open_node(node);
                $(".file-content").html(responseString);
                $(".file-content").attr("data-folder", fileFilename);
                //console.log(responseString);
            }
        });

        $.post("index.php?page=executeListAll", {
            pid: $(".file-tree").attr("id")
        }, function (response) {


            $('.file-tree').jstree({
                'core': {
                    'data': $.parseJSON(response),
                    dblclick_toggle: false
                },
                "conditionalselect": function (node, event) {

                    $.ajax({
                        url: "index.php",
                        data: {
                            page: "executeGetFileContent",
                            filename: node.original.a_attr
                        },
                        success: function (responseString) {

                            //$('.file-tree').open_node(node);
                            $(".file-content").html(responseString);
                            $(".file-content").attr("data-folder", node.original.a_attr);
                            //console.log(responseString);
                        }
                    });
                },
                "plugins": ["conditionalselect"]
            });
        });
    }

    $(document).on("click", "a.folder", function (e, data) {

        var fileFilename = $(this).attr("data-filename");
        var file = $(this).attr("data-original-title").replace(/\s/g,"");

        $.ajax({
            url: "index.php",
            data: {
                page: "executeGetFileContent",
                filename: fileFilename
            },
            success: function (responseString) {

                //$('.file-tree').open_node(node);
                $(".file-content").html(responseString);
                $(".file-content").attr("data-folder", fileFilename);
                
                //console.log($('.file-tree').jstree().get_node($("#" + file)));
                $('.file-tree').jstree().open_node($("#" + file));
                
                //console.log(responseString);
            }
        });
    });

    $('.file-tree').on("select_node.jstree", function (e, data) {
        $('.file-tree').toggle_node(data.node);
    });

    

});


