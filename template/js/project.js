/* 
 * Copyright (C) 2015 Jérôme Dumas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

$(function () {

    $(document).on("click", "button.addFolder", function () {
        
        $.ajax({
            url: "index.php?page=executeAddFolder",
            data: {
                path : $(".file-content").attr("data-folder"),
                filename : $("input#name").val()
            },
            success: function (responseHTML) {
                $(".file-content").html(responseHTML);
                $("#modalDialog").modal('toggle');
                
                $('.file-tree').jstree().create_node('#' ,  { "id" : "ajson5", "text" : $("input#name").val() }, "last", function(){
                    console.log("done");
                 });
                
            }
        });
    });
    
    $(document).on("click","a.deleteFile",function(){
        
        $.ajax({
            url: "index.php?page=executedeleteFile",
            data: {
                path : $(".file-content").attr("data-folder"),
                filename : $(this).attr("filename")
            },
            success: function (responseHTML) {
                $(".file-content").html(responseHTML);                
            }
        });        
    });
    
    /**
     * Ajout d'un projet en AJAX via POPUP
     */
    $(document).on("click","button.buttonAddProject",function(){
        
        $.ajax({
            type:"POST",
            url: "index.php?page=executeAddProject",
            data: {
                name : $("input#name").val(),
                description : $("textarea#description").val()
            },
            success: function (responseHTML) {
                $("#content-sub-sidebar").html(responseHTML);
                $("#modalDialog").modal('toggle');
            }
        });        
    });
    
    /**
     * Edition d'un projet en AJAX via POPUP
     */
    $(document).on("click","button.buttonEditProject",function(){
        
        $.ajax({
            type:"POST",
            url: "index.php?page=executeUpdateProject",
            data: {
                pid : $("input#project_id").val(),
                name : $("input#name").val(),
                description : $("textarea#description").val()
            },
            success: function (responseHTML) {
                $("#content-sub-sidebar").html(responseHTML);
                $("#modalDialog").modal('toggle');
            }
        });
    });  
    
    $(document).on("click","a.download",function(){
        
        $("#folderAction").submit();
        return true;
        
    });
    
    
});
