<?php

define("PARAMETERS_URL" , LIVE_URL . "/modules/parameters/");
define("PARAMETERS_VIEWS" , BASE_URL . "/modules/parameters/views/");

$config["parameters"] = array(
    "icon" => "fa fa-gear",
    "menu" => array(
        "Paramètres" => PARAMETERS_URL
    )
);
