<?php

require_once '../../libraries/SFBoot.php';
require_once 'models/model.php';
require_once 'config.php';

/**
 * Class de gestion des paramètres
 *
 * @author Jérôme DUMAS
 */
class parameters extends SFModules {

    private $model = null;

    public function __construct(parametersModel $model) {
        parent::__construct();

        $this->model = $model;
    }

    public function index() {
        
        $this->breadcrumb->add('Paramètres', USERS_URL, 0);

        $data = array(
            "parameters" => array(),
            "view" => "parameters",
            "javascript" => array()
        );

        echo $this->viewHelper->fetch(PARAMETERS_VIEWS . "index.tpl", $data);
    }
    
    public function areYouOk(){
        echo $this->viewHelper->fetch(PARAMETERS_VIEWS . "index2.tpl");
    }

}
