<?php

require_once("controllers/controller.php");
require_once ("models/model.php");

// Injection de dépendance, On envoi l'objet modèle au controller. Just for Fun.
$myModule = new parameters( new parametersModel() );

$myModule->load($_REQUEST);

