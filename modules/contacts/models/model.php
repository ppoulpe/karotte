<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of parameters
 *
 * @author jdumas
 */
class contactsModel {
    
    private $con = null;

    public function __construct() {
        
        $db = Database::getInstance();
        $this->con = $db->getDbh();
    }
    
    public function findAll(){

        $stmt = $this->con->prepare("SELECT contacts.* FROM contacts WHERE status = 1 ORDER BY name ASC;");
        $stmt->execute();
        
        return $stmt->fetchAll(PDO::FETCH_ASSOC);      
    }    
    
    public function find($cid = null){

        $stmt = $this->con->prepare("SELECT contacts.* FROM contacts WHERE contacts_id = :contacts_id;");
        $stmt->bindParam(':contacts_id', $cid);
        $stmt->execute();
        
        return $stmt->fetch(PDO::FETCH_ASSOC);           
    }    
    
    public function insert($data = array()){
        
        if(!isset($data["name"]) || $data["name"] == "")
            return false;
        
        $stmt = $this->con->prepare("INSERT INTO contacts (contacts_id, name, surname, email, phone, cellphone, address, address_2, zip, city, notes, fk_projects) "
                . "VALUES ('', :name, :surname, :email, :phone, :cellphone, :address, :address_2, :zip, :city, :notes, :fk_projects );");
        
        $stmt->bindParam(':name', $data["name"]);
        $stmt->bindParam(':surname', $data["surname"]);
        $stmt->bindParam(':email', $data["email"]);
        $stmt->bindParam(':phone', $data["phone"]);
        $stmt->bindParam(':cellphone', $data["cellphone"]);
        $stmt->bindParam(':address', $data["address"]);
        $stmt->bindParam(':address_2', $data["address_2"]);
        $stmt->bindParam(':zip', $data["zip"]);
        $stmt->bindParam(':city', $data["city"]);
        $stmt->bindParam(':notes', $data["notes"]);
        $stmt->bindParam(':fk_projects', $data["fk_projects"]);
        $stmt->execute();   
        
        return $this->con->lastInsertId();        
    }
    
    public function update($data = array()){
        
        $stmt = $this->con->prepare("UPDATE contacts SET name = :name, surname = :surname, email = :email, phone = :phone, cellphone = :cellphone, address = :address, address_2 = :address_2, zip = :zip, city = :city, notes = :notes, fk_projects = :fk_projects WHERE contacts_id = :contacts_id;");
        
        $stmt->bindParam(':name', $data["name"]);
        $stmt->bindParam(':surname', $data["surname"]);
        $stmt->bindParam(':email', $data["email"]);
        $stmt->bindParam(':phone', $data["phone"]);
        $stmt->bindParam(':cellphone', $data["cellphone"]);
        $stmt->bindParam(':address', $data["address"]);
        $stmt->bindParam(':address_2', $data["address_2"]);
        $stmt->bindParam(':zip', $data["zip"]);
        $stmt->bindParam(':city', $data["city"]);
        $stmt->bindParam(':notes', $data["notes"]);
        $stmt->bindParam(':fk_projects', $data["fk_projects"]);
        $stmt->bindParam(':contacts_id', $data["contacts_id"]);
        $stmt->execute();   
        
        return $this->con->lastInsertId();        
    }    
    
    public function delete($data = array()){
        
        $stmt = $this->con->prepare("UPDATE contacts SET status = 0 WHERE contacts_id = :contacts_id;");
                
        $stmt->bindParam(':contacts_id', $data["contacts_id"]);
        $stmt->execute();   
        
        return $this->con->lastInsertId();        
    }    
    
    
}
