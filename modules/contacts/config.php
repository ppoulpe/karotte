<?php

define("CONTACTS_URL" , LIVE_URL . "/modules/contacts/");
define("CONTACTS_VIEWS" , BASE_URL . "/modules/contacts/views/");

$config["contacts"] = array(
    "icon" => "fa fa-users",
    "target" => "href",
    "menu" => array(
        "Contacts" => CONTACTS_URL
    ),
    "actions" => array(
        array(
            "html" => "<i class='fa fa-plus'></i> Ajouter un contact",
            "href" => "javascript:void(0);",
            "class" => "modalDialog",
            "ajax" => "index.php?page=getDialog&dialog=addContact"
        )
    )
);
