<?php

require_once '../../libraries/SFBoot.php';
require_once 'models/model.php';
require_once 'config.php';

/**
 * Class de gestion des paramètres
 *
 * @author Jérôme DUMAS
 */
class contacts extends SFModules {

    private $model = null;

    public function __construct(contactsModel $model) {
        parent::__construct();

        $this->viewHelper->setGlobalAssign(array(
            "token" => $this->controllerHelper->_setToken("token"),
            "javascript" => array(                
                "wysihtml5-0.3.0.min",
                "bootstrap3-wysihtml5",
                "global",
                "contacts"
            ),
            "css" => array(
                "bootstrap-wysihtml5",
                "bootstrap3-wysiwyg5-color"
            )));        
        
        $this->model = $model;
    }

    public function index() {
        
        $this->breadcrumb->add('Contacts', CONTACTS_URL, 0);

        $data = array(
            "contacts" => $this->model->findAll(),
            "view" => "contacts"
        );

        $this->viewHelper->renderIntoLayout(CONTACTS_VIEWS . "index.tpl", $data);
    }
    
    public function getDialog() {
        
        $data = array();
        
        if($_REQUEST["cid"]){
            $data = $this->model->find($_REQUEST["cid"]);
        }
        
        echo $this->viewHelper->fetch(CONTACTS_VIEWS . "/dialogs/" . $_REQUEST["dialog"] . ".tpl", $data);
    }    
    
    public function executeFind(){        
        echo $this->viewHelper->fetch(CONTACTS_VIEWS . "/single.tpl", $this->model->find($_REQUEST["cid"]));
    }
    
    public function executeAddContact(){
        
        $post = array(
            "name" => $_REQUEST["name"],
            "surname" => $_REQUEST["surname"],
            "email" => $_REQUEST["email"],
            "cellphone" => $_REQUEST["phone"],
            "cellphone" => $_REQUEST["cellphone"],
            "address" => $_REQUEST["address"],
            "address_2" => $_REQUEST["address_2"],
            "zip" => $_REQUEST["zip"],
            "city" => $_REQUEST["city"],
            "notes" => $_REQUEST["notes"],
            "fk_projects" => $_REQUEST["fk_projects"],
        );
        
        if ($last_insert_id = $this->model->insert($post)) {

            Log::getInstance()->insert(array(
                "action" => "insert",
                "module" => "contacts",
                "title" => "Ajout d'un nouveau contact",
                "message" => "Un nouveau contact a été ajouté avec succès"
            ));
            
        } else {

            echo json_encode(array(
                "alert" => "Une erreur est survenue pendant l'ajout d'un nouveau contact"
            ));
        }        
    }
    
    public function executeUpdateContact(){
        
        $post = array(
            "name" => $_REQUEST["name"],
            "surname" => $_REQUEST["surname"],
            "email" => $_REQUEST["email"],
            "cellphone" => $_REQUEST["phone"],
            "cellphone" => $_REQUEST["cellphone"],
            "address" => $_REQUEST["address"],
            "address_2" => $_REQUEST["address_2"],
            "zip" => $_REQUEST["zip"],
            "city" => $_REQUEST["city"],
            "notes" => $_REQUEST["notes"],
            "fk_projects" => $_REQUEST["fk_projects"],
            "contacts_id" => $_REQUEST["contacts_id"]
        );
        
        if ($this->model->update($post)) {

            Log::getInstance()->insert(array(
                "action" => "update",
                "module" => "contacts",
                "title" => "Edition d'un contact",
                "message" => "Un contact a été édité avec succès"
            ));
            
        } else {

            echo json_encode(array(
                "alert" => "Une erreur est survenue pendant l'edition d'un contact"
            ));
        }        
    }   
    
    public function executeDeleteContact(){
        
        $data = array(
            "contacts_id" => $_REQUEST["cid"]
        );
        
        if ($this->model->delete($data)) {

            Log::getInstance()->insert(array(
                "action" => "delete",
                "module" => "contacts",
                "title" => "Suppression d'un contact",
                "message" => "Un contact a été supprimé avec succès"
            ));
            
        } else {

            echo json_encode(array(
                "alert" => "Une erreur est survenue pendant la suppression d'un contact"
            ));
        }        
    }     

}
