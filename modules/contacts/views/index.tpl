<div>
    <div class="col-sm-2 full-height sidebar">
        <div class='overlay'>
            <div class='row'>
            {section name=item loop=$contacts}
                
                    {if $fl_previous <> $contacts[item].name|truncate:1:""|upper}
                        <div class="col-sm-4 alphabeticalLetterChoice">
                            <a href="#{$contacts[item].name|truncate:1:""|upper}" class='filterByFirstLetter'>{$contacts[item].name|truncate:1:""|upper}</a>
                        </div>
                    {/if}
                {assign var="fl_previous" value=$contacts[item].name|truncate:1:""|upper}
            {/section}
            <div class="col-sm-4 alphabeticalLetterChoice">
                <a href="javascript:void(0)" class='closefilter'><i class="fa fa-arrow-left"></i></a>
            </div>
            </div>
        </div>
        {if $contacts}
            <ul class="spacing-list no-padding">
            {section name=item loop=$contacts}
                <li>
                    {if $fl_previous <> $contacts[item].name|truncate:1:""|upper}
                        <div class="alphabeticalLetter">
                            <a href="javascript:void(0)" id='{$contacts[item].name|truncate:1:""|upper}' class='firstLetter'>{$contacts[item].name|truncate:1:""|upper}</a>
                        </div>
                    {/if}
                    <a href="javascript:void(0);" id="{$contacts[item].contacts_id}" class="find">{$contacts[item].name|upper} {$contacts[item].surname}</a>
                </li>
                {assign var="fl_previous" value=$contacts[item].name|truncate:1:""|upper}
            {/section}
            </ul>
        {/if}
    </div>
        <div class="col-sm-10 full-height file-content-container">
            <div style="position:absolute;text-align:center;top:30%;left:50%;"><i class="fa fa-users" style="opacity: 0.03;font-size:18em;height:18em;"></i></div>
        </div>
</div>