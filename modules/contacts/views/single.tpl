<div class="row">
    <div class="col-sm-12">
        <h1>
            {$name} {$surname} 
            <small>
                <a href="javascript:void(0);" class="btn btn-primary modalDialog" data-ajax="index.php?page=getDialog&dialog=editContact&cid={$contacts_id}"><i class="fa fa-pencil"></i></a>
                <a href="javascript:void(0);" class="btn btn-danger deleteContact" id="{$contacts_id}"><i class="fa fa-trash"></i></a>
            </small>
        </h1>        
    </div>
    
    <div class="col-sm-12"><a href="mailto:{$email}">{$email}</a></div>
    <div class="col-sm-12">
        <span>{$phone}</span>
        <span>{$cellphone}</span>
    </div>
    <div class="col-sm-12">
        <span>{$address}</span>
        <span>{$address_2}</span>
    </div>
    <div class="col-sm-12">
        <span>{$zip}</span>
        <span>{$city}</span>
    </div>
    {if $notes}
    <div class="col-sm-12">
        <p>
            <i class="fa fa-quote-left"></i>
            {$notes}
            <i class="fa fa-quote-right"></i>
        </p>
    </div>
    {/if}
</div>