<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="gridSystemModalLabel">Ajouter un contact</h4>
        </div>
        <div class="modal-body">
            <form role="form" class="form-horizontal" name="addContactForm" id="addContactForm" method="post">
                <div class="form-group">
                    <div class="col-md-6">
                        <input type="text" title="Nom du contact" data-placement="bottom" data-toggle="tooltip" placeholder="Nom du contact" class="form-control" data-original-title="Nom du contact" id='name' name='name' required="required">
                    </div>   
                    <div class="col-md-6">
                        <input type="text" title="Prénom du contact" data-placement="bottom" data-toggle="tooltip" placeholder="Prénom du contact" class="form-control" data-original-title="Prénom du contact" id='surname' name='surname'>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6">
                        <input type="text" title="Mail du contact" data-placement="bottom" data-toggle="tooltip" placeholder="Mail du contact" class="form-control" data-original-title="Email du contact" id='email' name='email'>
                    </div>   
                    <div class="col-md-3">
                        <input type="text" title="Téléphone du contact" data-placement="bottom" data-toggle="tooltip" placeholder="Téléphone" class="form-control" data-original-title="Téléphone du contact" id='phone' name='phone'>
                    </div>                
                    <div class="col-md-3">
                        <input type="text" title="Portable du contact" data-placement="bottom" data-toggle="tooltip" placeholder="Portable" class="form-control" data-original-title="Portable du contact" id='cellphone' name='cellphone'>
                    </div>                   
                </div>
                <div class="form-group">
                    <div class="col-md-6">
                        <input type="text" title="Adresse du contact" data-placement="bottom" data-toggle="tooltip" placeholder="Adresse du contact" class="form-control" data-original-title="Adresse du contact" id='address' name='address'>
                    </div>   
                    <div class="col-md-6">
                        <input type="text" title="Adresse 2 du contact" data-placement="bottom" data-toggle="tooltip" placeholder="Adresse 2 du contact" class="form-control" data-original-title="Adresse 2 du contact" id='address_2' name='address_2'>
                    </div>                                  
                </div>    
                <div class="form-group">
                    <div class="col-md-6">
                        <input type="text" title="CP du contact" data-placement="bottom" data-toggle="tooltip" placeholder="CP" class="form-control" data-original-title="CP du contact" id='zip' name='zip'>
                    </div>   
                    <div class="col-md-6">
                        <input type="text" title="Ville du contact" data-placement="bottom" data-toggle="tooltip" placeholder="Ville du contact" class="form-control" data-original-title="Ville du contact" id='city' name='city'>
                    </div>                                  
                </div>   
                <div class="form-group">
                    <div class="col-sm-12">
                        <textarea class="form-control wysihtml5" id='notes' name='notes' rows="5"></textarea>
                    </div>
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-warning" data-dismiss="modal">Annuler</button>
            <button type="button" class="btn btn-success buttonAddContact">OK</button>
        </div>
    </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->

<script type="text/javascript">
    $(function () {
        $('.wysihtml5').wysihtml5();
    });
</script>