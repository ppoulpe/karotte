<?php

require_once("controllers/controller.php");
require_once ("models/model.php");

// Injection de dépendance, On envoi l'objet modèle au controller. Just for Fun.
$myModule = new contacts( new contactsModel() );

$myModule->load($_REQUEST);

