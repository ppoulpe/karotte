<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require_once '../../libraries/SFBoot.php';
require_once 'models/model.php';
require_once 'config.php';

/**
 * Description of root
 *
 * @author jdumas
 */
class dashboard extends SFModules {

    private $model = null;
    
    public function __construct() {
        parent::__construct();
        
        $this->model = new dashboardModel();
        
    }

    public function index() {
        
        $this->breadcrumb->add('Tableau de bord', USERS_URL, 0);
        
        $usersChartsArrayPdo = $this->model->getChartsOfUsers();
        $contentsChartsArrayPdo = $this->model->getChartsOfContents();       
        
        foreach ($usersChartsArrayPdo as $value) {
            $usersCharts[$value["group_id"]] = $value["charts"];
        }

        $data = array(
            "view" => "dashboard",
            "usersCharts" => $usersCharts,
            "contentsCharts" => $contentsCharts,
            "javascript" => array(
                "Chart.min",
                "dashboard.charts",
            )
        );

        $this->viewHelper->renderIntoLayout(DASHBOARD_VIEWS . "index.tpl", $data);
    }

}
