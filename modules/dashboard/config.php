<?php

define("DASHBOARD_URL" , LIVE_URL . "/modules/dashboard/");
define("DASHBOARD_VIEWS" , BASE_URL . "/modules/dashboard/views/");

$config["dashboard"] = array(
    "icon" => "fa fa-tachometer",
    "menu" => array(
        "Tableau de bord" => DASHBOARD_URL
    )
);
