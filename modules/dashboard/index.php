<?php

require_once("controllers/controller.php");

$myDashboardModule = new dashboard();

if(!$myDashboardModule->verifyConnexion() && $_REQUEST["page"] != "executeLogin"){
    header("Location:".LIVE_URL."/modules/users/");
}

$myDashboardModule->index();

