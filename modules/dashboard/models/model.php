<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of users
 *
 * @author jdumas
 */
class dashboardModel {
    
    private $con = null;

    public function __construct() {
        
        $db = Database::getInstance();
        $this->con = $db->getDbh();
    }
    
    public function getChartsOfUsers(){
        
        $stmt = $this->con->prepare("SELECT count(*) AS charts, groups.group_id, groups.name AS groupName "
                . "FROM users "
                . "INNER JOIN groups ON users.group_id = groups.group_id "
                . "GROUP BY group_id ;");
        
       $stmt->execute();
       
       return $stmt->fetchAll(PDO::FETCH_ASSOC);
        
    }
    
    public function getChartsOfContents(){
        
        $stmt = $this->con->prepare("SELECT count(*) AS charts, category.title AS categoryTitle "
                . "FROM content "
                . "INNER JOIN category ON content.fk_category = category.id "
                . "GROUP BY fk_category ;");
        
       $stmt->execute();
       
       return $stmt->fetchAll(PDO::FETCH_ASSOC);
        
    }    
    
}
