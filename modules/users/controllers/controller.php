﻿<?php

/* 
 * Copyright (C) 2014 Jérôme Dumas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

require_once '../../libraries/SFBoot.php';
require_once 'config.php';

/**
 * Controller des utilisateurs. Gère l'appel des vues et du modèle.
 *
 * @author Jérôme DUMAS
 */
class users extends SFModules {

    private $model = null;

    
    public function __construct(usersModel $model) {
        parent::__construct();

        $this->model = $model;
        $this->viewHelper->setGlobalAssign(array(
            "token" => $this->controllerHelper->_setToken("token"),
            "configuration" => $config["users"]));
    }

    public function index($alert = null) {
        
        // Ajout dans le fil d'ariane
        $this->breadcrumb->add('Utilisateurs', USERS_URL, 0);
        $this->breadcrumb->add('Gérer les utilisateurs', USERS_URL, 1);

        $data = array(
            "users" => $this->model->getAllUsers(),
            "view" => "users",
            "alert" => $alert,
            "javascript" => array(
                "functions"
            )
        );
        
//        var_dump($data["users"]);exit();

        $this->viewHelper->renderIntoLayout(USERS_VIEWS . "index.tpl", $data);
    }

    public function groups($alert = null) {
        
        // Ajout dans le fil d'ariane
        $this->breadcrumb->add('Utilisateurs', USERS_URL, 0);
        $this->breadcrumb->add('Gérer les groupes', USERS_URL, 1);     

        $data = array(
            "groups" => $this->model->getAllGroups(),
            "alert" => $alert
        );


        $this->viewHelper->renderIntoLayout(USERS_VIEWS . "groups.tpl", $data);
    }
    
    public function addGroup($alert = null) {
        
        // Ajout dans le fil d'ariane
        $this->breadcrumb->add('Utilisateurs', USERS_URL, 0);
        $this->breadcrumb->add('Gérer les groupes', USERS_URL . "index.php?page=groups", 1);
        $this->breadcrumb->add('Ajouter un groupe', USERS_URL, 2); 

        $this->viewHelper->renderIntoLayout(USERS_VIEWS . "addGroup.tpl", array(
            "alert" => $alert
        ));
    }
    
    public function editGroup($data , $alert = null) {

        $dataSmarty = array(
            "group" => $this->model->getOneGroup($data["group_id"]),
            "alert" => $alert
        );

        $this->viewHelper->renderIntoLayout(USERS_VIEWS . "editGroup.tpl", $dataSmarty);
    }    

    /**
     * @desc Charge la vue d'ajout d'un utilisateur
     * @param type $alert
     * @return <void>
     */
    public function addUser($alert = null) {
        
        $this->breadcrumb->add('Utilisateurs', USERS_URL, 0);
        $this->breadcrumb->add('Gérer les utilisateurs', USERS_URL, 1);
        $this->breadcrumb->add('Ajouter un utilisateur', USERS_URL, 2);

        $this->viewHelper->renderIntoLayout(USERS_VIEWS . "addUser.tpl", array(            
            "groups" => $this->model->getAllGroups(),
            "alert" => $alert
        ));
    }

    public function editUser($data , $alert = null) {

        $this->breadcrumb->add('Utilisateurs', USERS_URL, 0);
        $this->breadcrumb->add('Éditer un utilisateur', USERS_URL, 1);

        $dataSmarty = array(
            "alert" => $alert,
            "groups" => $this->model->getAllGroups(),
            "user" => $this->userHelper->manageUser($data["ID"])
            
        );

        $this->viewHelper->renderIntoLayout(USERS_VIEWS . "editUser.tpl", $dataSmarty);
    }

    public function login($alert = null) {

        $data = array(
            "alert" => $alert
        );

        $this->viewHelper->renderIntoLayout(USERS_VIEWS . "login.tpl", $data , "guestLayout.tpl");
    } 
    
    public function getSidebar(){
        echo $this->viewHelper->fetch(USERS_VIEWS . "sidebar.tpl");
    }

    /**
     * @desc Connecte un membre à la plateforme
     * @param <array> de <string> Informations de connexions (Login & Mot de passe)
     * @return <boolean> Affiche la vue et le message en fonction du résultat et retourne true ou false.
     */
    public function executeLogin($data) {
        
        ($data["remember"] == "on") ? $remember = true : $remember = false;

        if ($this->userHelper->login($data["username"], $data["password"], $remember)) {
            // On vérifie qu'on est bien en présence d'un administrateur
            if($this->userHelper->getProperty("GroupID") != 2){
                
                $this->login("Vous n'êtes pas un administrateur." . $this->userHelper->getProperty("Username"));
                return false;                
            }else{
                
                //$this->index();
                $this->viewHelper->_goTo(LIVE_URL);
                return true;                
            }
            
        } else {
            
            // On prépare le message d'erreur. On fait une boucle sur le tableau d'erreurs que nous renvoi la class uflex
            foreach ($this->userHelper->log->getErrors() as $value) {
                $alert .= $value;
            }
            
            $this->login($alert);

            return false;
        }
        return false;
    }

    /**
     * @desc Connecte un membre à la plateforme
     * @param <array> de <string> Informations de connexions (Login & Mot de passe)
     * @return <boolean> Affiche la vue et le message en fonction du résultat et retourne true ou false.
     */
    public function executeLogout() {

        $this->userHelper->logout();
        $this->login();
        return true;
        
    }

    /**
     * @desc Ajoute un utilisateur
     * @param <array> - Informations de l'utilisateur à rajouter
     */
    public function executeAddUser($data) {
        
        // On doit cloner l'instance de l'utilisateur en cours pour pouvoir créer un nouvel utilisateur
        $nuser = $this->userHelper->manageUser($_SESSION["userData"]["data"]["ID"]); 
        $nuser->signed = false;

        if ($nuser->register($data)) {
            $this->index();
        } else{
            $this->addUser($data , $this->userHelper->log->getErrors());
        }
    }

    /**
     * @desc Modifie un utilisateur. 
     * @param <array> - Données de l'utilisateur à modifier
     */
    public function executeUpdateUser($data) {
        
        // On clone l'instance de l'utilisateur a modifier.
        $nuser = $this->userHelper->manageUser($data["ID"]);
        
        // On compare les données, pour ne pas avoir d'erreur de type "Email déjà prit". Nécessaire pour la classe uFlex
        if ($nuser->Email == $data["Email"]) unset($data["Email"]);
        if ($nuser->Username == $data["Username"]) unset($data["Username"]);
        if (!$data["Password"]) {
            
            unset($data["Password"]); 
            unset($data["Password2"]);
            
        }

        if ($nuser->update($data)) {

            $this->index();
            
        } else {
            
            // On prépare le message d'erreur. On fait une boucle sur le tableau d'erreur que nous renvoi la class uflex
            foreach ($nuser->log->getErrors() as $key => $value) {
                $alert .= $value;
            }
            
            $this->editUser($data , $alert);
        }
    }

    /**
     * @desc Supprimer un utilisateur
     * @param <array> of <integer> Contient l'id de la personne à supprimer
     */
    public function executeDeleteUser($data) {

        if ($this->model->deleteUser($data)) {
            $alert = "Le membre a été supprimé avec succès.";
        }
        
        $this->index($alert);
    }
    
    /**
     * @desc Permet d'activer ou de désactiver un utilisateur via le panneau d'administration.
     * @param <array> of <integer> Contient l'id de la personne à activer/désactiver
     */
    public function executeSwitchState($data){
        
        $nuser = $this->userHelper->manageUser($data["ID"]);
        
        if($nuser->Activated == 1){
            $nuser->update(array("Activated" => 0));
            $alert = "Le membre " . $nuser->Username . " a été desactivé avec succès.";
        }else{
            $nuser->update(array("Activated" => 1));
            $alert = "Le membre " . $nuser->Username . " a été activé avec succès.";
        }
        
        $this->index($alert);
    }
    
    public function executeAddGroup($data){
        
        if($this->model->addGroup($data["name"]) < 1){
            $alert = "Un erreur est survenue lors de l'enregistrement du nouveau groupe.";
            $this->addGroup($data,$alert);
            return false;
        }
        
        $this->groups("Le groupe a été crée avec succès!");
        return true;
    }
    
    public function executeUpdateGroup($data) {

        if ($this->model->updateGroup($data) < 1) {
            
            $this->editGroup($data, "Une erreur est survenue pendant l'édition du groupe.");
            return false;
            
        }else{
            $this->groups("Le groupe a été édité avec succès.");
            return true;
        }
    }    
    
    /**
     * @desc Supprimer un groupe
     * @param <array> of <integer> Contient l'id du groupe à supprimer
     */
    public function executeDeleteGroup($data) {

        if ($this->model->deleteGroup($data["group_id"]) < 1) {
            $alert = "Une erreur est survenue pendant la suppression du groupe.";
            
        }else{
            $alert = "Le groupe a été supprimé avec succès.";
        }
        
        $this->groups($alert);
    }    

}

