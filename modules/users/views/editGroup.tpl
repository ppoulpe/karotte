<form action="index.php?page=executeUpdateGroup" method="post" id="editGroupForm" name="editGroupForm" class='form-horizontal' role="form">

    <input type="hidden" value="{$group.group_id}" id='group_id' name='group_id' required="required">

    <div class="form-group">
        <label class="col-sm-1 control-label">Nom du groupe</label>
        <div class="col-sm-2">
            <input type="text" value="{$group.name}" data-placement="bottom" data-toggle="tooltip" placeholder="name" class="form-control" data-original-title="Nom du groupe" id='name' name='name' required="required">
        </div>                 
    </div>        
    <div class="form-group">

        <div class="col-sm-2 col-sm-offset-1">
            <input type='submit' value='Sauvegarder' class='btn btn-success'/>
        </div>
    </div>
</form>