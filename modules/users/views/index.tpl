<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <div class="table-responsive">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th></th>
                        <th>Identifiant</th> 
                        <th>Prénom - nom</th>
                        <th>Inscription</th> 
                        <th>Dernière connexion</th>
                        <th>Groupe</th> 
                        <th class="text-right">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    {section name=item loop=$users}
                        <tr>
                            <td><a href="index.php?page=executeSwitchState&ID={$users[item].ID}"><div class="left activated-{$users[item].Activated}">{$users[item].Activated}</div></a></td>
                            <td><a href="index.php?page=editUser&ID={$users[item].ID}">{$users[item].Username}</a></td>
                            <td>{$users[item].surname} {$users[item].name}</td>
                            <td>{$users[item].RegDate|date_format:"le %d/%m/%Y"}</td>
                            <td>{$users[item].LastLogin|date_format:"le %d/%m/%Y à %H:%M"}</td>
                            <td>{$users[item].groupName}</td>
                            <td class="text-right">
                                <a href="index.php?page=editUser&ID={$users[item].ID}">
                                    <i class="fa fa-pencil"></i>
                                </a>
                                <a class="confirm" title="Êtes-vous sûr de vouloir supprimer le membre {$users[item].Username} ?" href="index.php?page=executeDeleteUser&uid={$users[item].ID}">
                                    <i class="fa fa-trash-o"></i>
                                </a>
                            </td>                    
                        </tr>
                    {/section}

                </tbody>

            </table>           
            <hr />
            <p class="text-right"><a href="index.php?page=addUser" class="btn btn-success">Ajouter un utilisateur</a></p>
        </div>
    </div>
</div>
<!-- /.row -->
