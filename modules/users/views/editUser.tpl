<form action="index.php?page=executeUpdateUser" method="post" id="editUserForm" name="editUserForm" class='form-horizontal' role="form">

    <input type="hidden" value="{$user->ID}" id='ID' name='ID' required="required">

    <div class="form-group">
        <label class="col-sm-1 control-label">Identifiant</label>
        <div class="col-sm-2">
            <input type="text" value="{$user->Username}" data-placement="bottom" data-toggle="tooltip" placeholder="Identifiant" class="form-control" data-original-title="Identifiant" id='Username' name='Username' required="required">
        </div>                 
    </div>
    <div class='form-group'>
        <label class="col-sm-1 control-label">E-mail</label>
        <div class="col-sm-2">
            <input type="text" value="{$user->Email}" data-placement="bottom" data-toggle="tooltip" placeholder="E-mail" class="form-control" data-original-title="E-mail" id='Email' name='Email' required="required">
        </div>                       
    </div>
    <div class="form-group">
        <label class="col-sm-1 control-label">Mot de passe</label>
        <div class="col-sm-2">
            <input type="password" data-placement="bottom" data-toggle="tooltip" placeholder="Mot de passe" class="form-control" data-original-title="Mot de passe" id='Password' name='Password' >
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-1 control-label">Nom</label>
        <div class="col-sm-2">
            <input type="text" value="{$user->name}" data-placement="bottom" data-toggle="tooltip" placeholder="Nom" class="form-control" data-original-title="Nom" id='name' name='name'>
        </div>
    </div>    
    <div class='form-group'>
        <label class="col-sm-1 control-label">Prénom</label>
        <div class="col-sm-2">
            <input type="text" value="{$user->surname}" data-placement="bottom" data-toggle="tooltip" placeholder="Prénom" class="form-control" data-original-title="Prénom" id='surname' name='surname'>
        </div>                    
    </div>    
    <div class='form-group'>
        <label class="col-sm-1 control-label">Groupe</label>
        <div class="col-sm-2">
            <select id="GroupID" name="GroupID">
                {section name=group loop=$groups}
                    <option value="{$groups[group].group_id}" {if $user->GroupID == $groups[group].group_id}selected="selected"{/if}>{$groups[group].name}</option>
                {/section}
            </select>
        </div>                    
    </div>                     
    <div class="form-group">

        <div class="col-sm-2 col-sm-offset-1">
            <input type='submit' value='Sauvegarder' class='btn btn-success'/>
        </div>
    </div>
</form>