
<div class="container-fluid">
    <div id="page-login" class="row">
        <div class="col-xs-12 col-md-2 col-md-offset-5 col-sm-6 col-sm-offset-3">
            <div class="box">
                <div class="box-content">
                    <form action="index.php?page=executeLogin" method="post">
                        <div class="text-center">
                            <h3 class="page-header">Page de connexion</h3>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Identifiant</label>
                            <input type="text" class="form-control" id="username" name="username" />
                        </div>
                        <div class="form-group">
                            <label class="control-label">Mot de passe</label>
                            <input type="password" class="form-control" id="password" name="password" />
                        </div>
                        <div class="text-center">
                            <input type="submit" class="btn btn-primary" value="Connexion" />
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>