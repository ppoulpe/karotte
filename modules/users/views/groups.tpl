<table class="table table-bordered table-striped table-hover table-heading table-datatable dataTable">
    <thead>
        <tr>
            <th>Identifiant</th> 
            <th>Nom</th> 
            <th class="text-right">Actions</th> 
        </tr>
    </thead>
    <tbody>
        {section name=item loop=$groups}
            <tr>
                <td>{$groups[item].group_id}</td>
                <td><a href="index.php?page=editGroup&group_id={$groups[item].group_id}">{$groups[item].name}</a></td>
                <td class="text-right">
                    <a href="index.php?page=editGroup&group_id={$groups[item].group_id}">
                        <i class="fa fa-pencil"></i>
                    </a>
                    <a class="confirm" title="Êtes-vous sûr de vouloir supprimer le groupe {$groups[item].name} ?" href="index.php?page=executeDeleteGroup&group_id={$groups[item].group_id}">
                        <i class="fa fa-trash-o"></i>
                    </a>
                </td>
            </tr>
        {/section}

    </tbody>

</table>           
<hr />
<p class="text-right"><a href="index.php?page=addGroup" class="btn btn-success">Ajouter un groupe</a></p>