<form action="index.php?page=executeAddGroup" method="post" id="addGroupForm" name="addGroupForm" class='form-horizontal' role="form">

    <div class="form-group">
        <label class="col-sm-1 control-label">Nom du groupe</label>
        <div class="col-sm-2">
            <input type="text" data-placement="bottom" data-toggle="tooltip" placeholder="name" class="form-control" data-original-title="Nom du groupe" id='name' name='name' required="required">
        </div>                 
    </div>        
    <div class="form-group">

        <div class="col-sm-2 col-sm-offset-1">
            <input type='submit' value='Sauvegarder' class='btn btn-success'/>
        </div>
    </div>
</form>