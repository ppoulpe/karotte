<?php

define("USERS_URL" , LIVE_URL . "/modules/users/");
define("USERS_VIEWS" , BASE_URL . "/modules/users/views/");

$config["users"] = array(
    "icon" => "fa fa-users",
    "menu" => array(
        "Utilisateurs" => USERS_URL . "index.php?page=getSidebar" /*array(
            "Gérer les utilisateurs" => USERS_URL,
            "Gérer les groupes" => USERS_URL . "index.php?page=groups"
        )*/
    )
);

configuration::getInstance()->setConfiguration("menus" , array(
    "name" => "users",
    "icon" => "fa fa-users",
    "menu" => array(
        "Utilisateurs" => USERS_URL . "/index.php?page=getSidebar"
    )
));
