<?php header ('Content-type: text/html; charset=UTF-8'); 

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of users
 *
 * @author jdumas
 */
class usersModel {

    private $con = null;

    public function __construct() {

        $db = Database::getInstance();
        $this->con = $db->getDbh();
    }

    public function getAllUsers() {

        $stmt = $this->con->prepare("SELECT users.* , groups.name AS groupName FROM Users INNER JOIN groups ON users.GroupID = groups.group_id;");
        $stmt->execute();
        
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getAllGroups() {

        $stmt = $this->con->prepare("SELECT * FROM groups;");
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    
    public function getOneGroup($idOfGroup = null) {

        $stmt = $this->con->prepare("SELECT group_id, name FROM groups WHERE group_id = :group_id;");
        
        $stmt->bindParam(':group_id', $idOfGroup);
        $stmt->execute();

        return $stmt->fetch(PDO::FETCH_ASSOC);
    }    
    
    public function updateGroup($data) {

        $stmt = $this->con->prepare("UPDATE groups SET name = :name WHERE group_id = :group_id;");
        
        $stmt->bindParam(':name', $data["name"]);
        $stmt->bindParam(':group_id', $data["group_id"]);
        
        return $stmt->execute();
    }    
    
    public function addGroup($nameOfGroup){
        $stmt = $this->con->prepare("INSERT INTO groups (group_id, name) VALUES ('',:name);");
        
        $stmt->bindParam(':name', $nameOfGroup);
        
        return $stmt->execute();
    }
    
    public function deleteGroup($idOfGroup) {

        $stmt = $this->con->prepare("DELETE FROM groups WHERE group_id = :group_id;");
        
        $stmt->bindParam(':group_id', $idOfGroup);
        
        return $stmt->execute();
    }    

    public function deleteUser($data) {

        $stmt = $this->con->prepare("DELETE FROM users WHERE ID = :uid;");
        
        $stmt->bindParam(':uid', $data["uid"]);
        
        return $stmt->execute();
    }

}
