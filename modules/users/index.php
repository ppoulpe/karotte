<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require_once("controllers/controller.php");
require_once ("models/model.php");

// Injection de dépendance, On envoi l'objet modèle au controller. Just for Fun.
$myUsersModule = new users( new usersModel() );

$myUsersModule->load($_REQUEST);
