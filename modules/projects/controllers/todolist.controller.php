<?php

/* 
 * Copyright (C) 2015 Jérôme Dumas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

require_once '../../libraries/SFBoot.php';
require_once 'config.php';

/**
 * Description of root
 *
 * @author jdumas
 */
class todolist extends SFModules {

    private $model = null;

    public function __construct(todolistModel $model) {

        parent::__construct();

        $this->model = $model;
        
        $this->viewHelper->setGlobalAssign(array(
            "token" => $this->controllerHelper->_setToken("token"),
            "configuration" => configuration::getInstance()->getConfiguration(),
            "javascript" => array(
                "wysihtml5-0.3.0.min",
                "bootstrap3-wysihtml5",
                "global"
            ),
            "css" => array(
                "bootstrap-wysihtml5",
                "bootstrap3-wysiwyg5-color",
                "plugins/tree/themes/default/style.min"
        )));
    }
    
    public function index($data = array()){
        
        $this->breadcrumb->add('A faire', PROJECTS_URL, 0);

        //$data["todolist"] = $this->model->getAllTodo();

        $this->viewHelper->renderIntoLayout(PROJECTS_VIEWS . "/todolist/index.tpl", $data);        
    }
}