<?php

/*
 * Copyright (C) 2015 Jérôme Dumas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

require_once '../../libraries/SFBoot.php';
require_once 'config.php';

/**
 * Description of root
 *
 * @author jdumas
 */
class projects extends SFModules {

    private $model = null;
    private $todolist = null;

    //private $configuration = $config["projects"];

    public function __construct(projectsModel $model) {

        parent::__construct();

        
        $this->model = $model;        
        //$this->todolist = new todolist(new todolistModel()); // On initialize la todolist pour le projet
        
        $this->viewHelper->setGlobalAssign(array(
            "token" => $this->controllerHelper->_setToken("token"),
            "configuration" => configuration::getInstance()->getConfiguration(),
            "javascript" => array(
                "wysihtml5-0.3.0.min",
                "bootstrap3-wysihtml5",
                "plugins/tree/jstree.min",
                "plugins/dropzone/dropzone",
                "global",
                "project",
                "filesmanager"
            ),
            "css" => array(
                "bootstrap-wysihtml5",
                "bootstrap3-wysiwyg5-color",
                "plugins/tree/themes/default/style.min"
            /* "plugins/dropzone/dropzone" */
        )));
    }

    public function index($data = array()) {

        $this->breadcrumb->add('Mes projets', PROJECTS_URL, 0);

        $data["projects"] = $this->model->getAllProjects();

        $this->viewHelper->renderIntoLayout(PROJECTS_VIEWS . "index.tpl", $data);
    }
    
    public function todolist($data = array()){
        $this->breadcrumb->add('A faire - 2', PROJECTS_URL, 0);

        $this->viewHelper->renderIntoLayout(PROJECTS_VIEWS . "/todolist/index.tpl", $data);
    }

    public function addProject($data = array()) {

        $this->breadcrumb->add('Mes projets', PROJECTS_URL, 0);
        $this->breadcrumb->add('Ajouter un nouveau projet', "#", 1);

        $this->viewHelper->renderIntoLayout(PROJECTS_VIEWS . "addProject.tpl", $data);
    }

    public function editProject($data = array()) {

        Files::createFolder(BASE_URL . '/documents/projets/project_' . $_REQUEST["pid"] . '/ressources/');

        $data["project"] = $this->model->get($_REQUEST["pid"]);

        $this->breadcrumb->add('Mes projets', PROJECTS_URL, 0);
        $this->breadcrumb->add($data["project"]["name"], "#", 1);
        $this->breadcrumb->add($_REQUEST["view"], "#", 2);


        $data["entries"] = Files::list_all(BASE_URL . '/documents/projets/project_' . $_REQUEST["pid"] . '/ressources/');

        $this->viewHelper->renderIntoLayout(PROJECTS_VIEWS . $_REQUEST["view"] . ".tpl", $data);
    }
    

    public function deleteProject($data = array()) {

        $this->executeDeleteProject($_REQUEST["pid"]);
    }

    public function getSidebar() {

        $data = array(
            "projects" => $this->model->getAllProjects()
        );

        echo $this->viewHelper->fetch(PROJECTS_VIEWS . "sidebar.tpl", $data);
    }

    public function getDialog() {

        $data = array(
            "currentFolder" => $_REQUEST["currentFolder"]
        );

        if ($_REQUEST["pid"])
            $data["project"] = $this->model->get($_REQUEST["pid"]);

        echo $this->viewHelper->fetch(PROJECTS_VIEWS . "/dialogs/" . $_REQUEST["dialog"] . ".tpl", $data);
    }

    /**
     * @desc Télécharge un fichier ou une archive zippé contenant plusieurs fichiers
     * @return <boolean>
     */
    public function downloadFiles() {

        (string) $filename = "Archive.zip";
        $filesToZip = array();

        if (count($_REQUEST["action_chkbx"]) > 1) {

            foreach ($_REQUEST["action_chkbx"] as $key => $value) {
                $fileJSON = json_decode($value);
                $filesToZip[$key]["folder"] = $fileJSON->folder;
                $filesToZip[$key]["file"] = $fileJSON->file;
            }

            $zipfile = Files::createArchive($filesToZip, BASE_URL . "/tmp/" . $filename);
        } else {
            $fileJSON = json_decode($_REQUEST["action_chkbx"][0]);
            $zipfile = $fileJSON->folder . "/" . $fileJSON->file;
            $filename = $fileJSON->file;
        }

        if ($zipfile) {

            header("Content-disposition: attachment; filename=" . $filename);
            header("Content-Type: application/force-download");
            header("Content-Transfer-Encoding: application/octet-stream");
            header("Content-Length: " . filesize($zipfile));
            header("Pragma: no-cache");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0, public");
            header("Expires: 0");

            readfile($zipfile);

            unlink(BASE_URL . "/tmp/" . $filename);

            return true;
        } else {
            return false;
        }
    }

    public function downloadFile() {
                
        $zipfile = $_REQUEST["filepath"] . "/" . $_REQUEST["file"];
        $filename = $_REQUEST["file"];

        if ($zipfile) {

            header("Content-disposition: attachment; filename=" . $filename);
            header("Content-Type: application/force-download");
            header("Content-Transfer-Encoding: application/octet-stream");
            header("Content-Length: " . filesize($zipfile));
            header("Pragma: no-cache");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0, public");
            header("Expires: 0");

            readfile($zipfile);

            unlink(BASE_URL . "/tmp/" . $filename);

            return true;
        } else {
            return false;
        }
    }
    


    /**
     * @desc Cette fonction, appelée en AJAX, ajoute un projet avec les informations passée en paramètre
     * @param <array> Informations à insérer (Nom, Description & ID)
     */
    public function executeAddProject() {

        $projectName = ($_REQUEST["name"]) ? $_REQUEST["name"] : "Projet Doe";
        $projectDescription = ($_REQUEST["description"]) ? $_REQUEST["description"] : "Aucune description";

        if ($last_insert_id = $this->model->insert(array(
            "name" => $projectName,
            "description" => $projectDescription,
            //"picture" => $_FILES["picture"]['name'],
            "user_id" => $_SESSION["userData"]["data"]["ID"]
                ))) {

            // On crée le dossier du projet
            Files::createFolder(BASE_URL . '/documents/projets/project_' . $last_insert_id . '/ressources/');

            Log::getInstance()->insert(array(
                "action" => "insert",
                "module" => "projects",
                "title" => "Ajout d'un nouveau projet",
                "message" => "Un nouveau projet a été ajouté avec succès"
            ));

            $this->getSidebar();
        } else {

            echo json_encode(array(
                "alert" => "Une erreur est survenue pendant l'ajout d'un nouveau projet"
            ));
        }
    }

    /**
     * @desc Cette fonction, appelée en AJAX, met à jour un projet avec les informations passée en paramètre
     * @param <array> Informations à mettre à jour (Nom, Description & ID)
     */
    public function executeUpdateProject($data = array()) {

        $data["project_id"] = $_REQUEST["pid"];
        if ($_REQUEST["name"])
            $data["name"] = $_REQUEST["name"];

        if ($_REQUEST["description"])
            $data["description"] = $_REQUEST["description"];

        if ($this->model->update($data)) {

            // Si succès, on insère dans les log l'action de l'utilisateur
            Log::getInstance()->insert(array(
                "action" => "update",
                "module" => "projects",
                "title" => "Mise à jour du projet #" . $data["project_id"],
                "message" => "Le projet #" . $data["project_id"] . "a été mis à jour !"
            ));

            // On renvoi ensuite la sidebar complète, pour refresh
            $this->getSidebar();
        } else {

            // Sinon, on renvoi l'erreur en JSON
            echo json_encode(array(
                "alert" => "Une erreur est survenue pendant la mise à jour du projet #" . $data["project_id"]
            ));
        }
    }

    public function executeDeleteProject($pid = null) {

        if ($this->model->delete($pid)) {

            Log::getInstance()->insert(array(
                "action" => "delete",
                "module" => "projects",
                "title" => "Suppression du projet #" . $data["project_id"],
                "message" => "Le projet #" . $data["project_id"] . " a été Supprimé !"
            ));

            $this->index(array(
                "alert" => "Le projet #" . $data["project_id"] . " a été Supprimé !"
            ));
        } else {

            $this->index(array(
                "alert" => "Une erreur est survenue pendant la Suppression du projet #" . $data["project_id"]
            ));
        }
    }

    /**
     * 
     */
    public function executeUploadFile() {

        if (Files::upload("file", $_REQUEST["folder"] . "/")) {

            Log::getInstance()->insert(array(
                "action" => "upload",
                "module" => "projects",
                "title" => "Envoi de fichier(s)",
                "message" => "Des fichiers ont été envoyés dans le dossier du projet #" . $data["project_id"]
            ));

            return true;
        } else {
            return false;
        }
    }

    /**
     * 
     * @return boolean
     */
    public function executeAddFolder() {

        if (Files::createFolder($_REQUEST["path"] . "/" . $_REQUEST["filename"])) {

            Log::getInstance()->insert(array(
                "action" => "upload",
                "module" => "projects",
                "title" => "Envoi de fichier(s)",
                "message" => "Un dossier a été crée pour le projet #" . $_REQUEST["pid"]
            ));

            echo $this->viewHelper->fetch(PROJECTS_VIEWS . "files.tpl", array(
                "files" => Files::getFileContent($_REQUEST["path"] . "/"),
                "folder" => $_REQUEST["path"]
            ));
        } else {

            echo json_encode(array(
                "alert" => "Une erreur est survenue pendant la création du dossier"
            ));
        }
    }
    
    /**
     * @desc supprime un fichier du serveur
     * @return <boolean>
     */
    public function executeDeleteFile(){
        
        if(unlink($_REQUEST["path"] . "/" . $_REQUEST["filename"])){
            Log::getInstance()->insert(array(
                "action" => "delete",
                "module" => "projects",
                "title" => "Suppression de fichier(s)",
                "message" => "Un fichier a été supprimé pour le projet #" . $_REQUEST["pid"]
            ));   
            
            echo $this->viewHelper->fetch(PROJECTS_VIEWS . "files.tpl", array(
                "files" => Files::getFileContent($_REQUEST["path"] . "/"),
                "folder" => $_REQUEST["path"]
            ));
            
        }else{
            echo json_encode(array(
                "alert" => "Une erreur est survenue pendant la suppression du fichier."
            ));            
        }
    }    
    
    /**
     * @desc Liste l'ensemble des fichiers contenus dans un
     */
    public function executeListAll() {
        echo json_encode(Files::list_all(BASE_URL . '/documents/projets/project_' . $_REQUEST["pid"]));
    }

    public function executeGetFileContent() {

        echo $this->viewHelper->fetch(PROJECTS_VIEWS . "files.tpl", array(
            "files" => Files::getFileContent($_REQUEST["filename"]),
            "folder" => $_REQUEST["filename"]
        ));
    }

    public function executeGetFileDetails() {

        echo $this->viewHelper->fetch(PROJECTS_VIEWS . "fileDetails.tpl", array(
            "details" => Files::getFileDetails($_REQUEST["filename"]),
            "folder" => $_REQUEST["filename"]
        ));
    }

}
