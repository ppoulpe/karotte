<?php

define("PROJECTS_URL" , LIVE_URL . "/modules/projects/");
define("PROJECTS_VIEWS" , BASE_URL . "/modules/projects/views/");

$config["projects"] = array(
    "icon" => "fa fa-folder-open-o",
    "menu" => array(
        "Mes projets" => PROJECTS_URL . "index.php?page=getSidebar"
    )
);

configuration::getInstance()->setConfiguration("projects" , array(
    "icon" => "fa fa-folder-open-o",
    "menu" => array(
        "Mes projets" => PROJECTS_URL . "index.php?page=getSidebar"
    )
));
