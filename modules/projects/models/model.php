<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of users
 *
 * @author jdumas
 */
class projectsModel {
    
    private $con = null;

    public function __construct() {
        
        $db = Database::getInstance();
        $this->con = $db->getDbh();
    }
    
    public function insert($data = array()){
        
        if(!isset($data["name"]) || $data["name"] == "")
            return false;
        
        $stmt = $this->con->prepare("INSERT INTO projects (project_id, name, picture, description, users_id) VALUES ('', :name, :picture, :description, :users_id);");
        
        $stmt->bindParam(':name', $data["name"]);
        $stmt->bindParam(':description', $data["description"]);
        $stmt->bindParam(':picture', $data["picture"]);
        $stmt->bindParam(':users_id', $_SESSION["userData"]["data"]["ID"]);
        $stmt->execute();   
        
        return $this->con->lastInsertId();
        
    }
    
    public function update($data = array()){
        
        if(!isset($data["name"]) || $data["name"] == "")
            return false;
        
        if(!isset($data["project_id"]) || $data["project_id"] == "")
            return false;
        
        $stmt = $this->con->prepare("UPDATE projects SET name = :name, description = :description, users_id = :users_id WHERE project_id = :project_id;");

        $stmt->bindParam(':project_id', $data["project_id"]);
        $stmt->bindParam(':name', $data["name"]);
        $stmt->bindParam(':description', $data["description"]);
        $stmt->bindParam(':users_id', $_SESSION["userData"]["data"]["ID"]);
        
        return $stmt->execute();
        
    }    
    
    public function delete($pid = null){
        
        if(!isset($pid) || $pid == "")
            return false;
        
        $stmt = $this->con->prepare("UPDATE projects SET statut = 0 WHERE project_id = :project_id;");

        $stmt->bindParam(':project_id', $pid);
        
        return $stmt->execute();
        
    }        
    
    public function get($pid = null){

        $stmt = $this->con->prepare("SELECT projects.* FROM projects WHERE project_id = :project_id;");
        $stmt->bindParam(':project_id', $pid);
        $stmt->execute();
        
        return $stmt->fetch(PDO::FETCH_ASSOC);           
    }
    
    public function getAllProjects(){

        $stmt = $this->con->prepare("SELECT projects.* FROM projects WHERE users_id = :users_id AND statut <> 0;");
        $stmt->bindParam(':users_id', $_SESSION["userData"]["data"]["ID"]);
        $stmt->execute();
        
        return $stmt->fetchAll(PDO::FETCH_ASSOC);      
    }
    
}
