<div class="row">
    <div class="col-xs-3">
        <img src="{$details.preview}" class="full-width"/>
    </div>
    <div class="col-xs-9">
        
        <ul class="no-padding">
            <li>Type : {$details.mime}</li>
            <li>Dernière modification le {$details.last_update|date_format:"%m/%d/%Y"} à {$details.last_update|date_format:"%H:%M"}</li>
            <li>{$details.size}</li>
        </ul>
        <p>
            <a href="index.php?page=downloadFile&file={$details.name}&filepath={$details.filepath}" class="btn btn-success"><i class="fa fa-download"></i></a>
            <a href="javascript:void(0);" class="btn btn-danger deleteFile" filename="{$details.name}"><i class="fa fa-trash"></i></a>
        </p>
    </div>
    
</div>