<table class="table table-bordered table-striped table-hover table-heading table-datatable dataTable">
    <thead>
        <tr>
            <th></th>
            <th>Nom du projet</th>
            <th class="text-right">Actions</th>
        </tr>
    </thead>
    <tbody>
        {if $projects|@count > 0}
            {section name=item loop=$projects}
                <tr>
                    <td>{$projects[item].project_id}</td>
                    <td>{$projects[item].name}</td>
                    <td>

                        <div class="btn-group">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Action <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                                <li><a href="index.php?page=editProject&pid={$projects[item].project_id}">Editer</a></li>
                                <li><a href="index.php?page=deleteProject&pid={$projects[item].project_id}">Supprimer</a></li>
                            </ul>
                        </div>                                    
                    </td>
                </tr>
            {/section}
        {else}
        <td colspan="3">Aucune donnée à afficher</td>
    {/if}
</tbody>

</table>           
<hr />
<p class="text-right">
    
    <a href="index.php?page=addProject" class="btn btn-success">Ajouter un projet</a>
</p>