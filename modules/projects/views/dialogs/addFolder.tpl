
<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="gridSystemModalLabel">Ajouter un dossier au projet</h4>
        </div>
        <div class="modal-body">
                <div class="row">
                        <div class="col-md-12">
                            <input type="text" title="Nom du dossier" data-placement="bottom" data-toggle="tooltip" placeholder="Nom du dossier" class="form-control" data-original-title="Nom du dossier" id='name' name='name' required="required">
                        </div>   
                </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-warning" data-dismiss="modal">Annuler</button>
            <button type="button" class="btn btn-success addFolder">OK</button>
        </div>
    </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->