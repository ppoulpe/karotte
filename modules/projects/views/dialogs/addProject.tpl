<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-plus"></i> Ajouter un nouveau projet</h4>
        </div>
        <div class="modal-body">
            <form action='index.php?token={$token.token_object}' method='post' id='addProjectForm' name='addProjectForm' class='form-horizontal' role="form">
                <div class="form-group">
                    <div class="col-sm-6">
                        <input type="text" title="Nom du projet" data-placement="bottom" data-toggle="tooltip" placeholder="Nom du projet" class="form-control" data-original-title="Nom du projet" id='name' name='name' required="required">
                    </div>                 
                </div>  
                <div class="form-group">
                    <div class="col-sm-12">
                        <textarea class="form-control wysihtml5" id='description' name='description' rows="5"></textarea>
                    </div>                 
                </div>                 
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-warning" data-dismiss="modal">Annuler</button>
            <button type="button" class="btn btn-success buttonAddProject">OK</button>
        </div>
    </div>
</div>
                
                <script type="text/javascript">
                    $(function(){
   $('.wysihtml5').wysihtml5(); 
});
                </script>