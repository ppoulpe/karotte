<div id="dropzone" class="row-file full-height padding" style="position:relative;">

    <form id="folderAction" name="folderAction" action="index.php?page=downloadFiles" method="post">
        {section name=item loop=$files}

            <div class="col-xs-6">
                <div class="col-xs-1">
                    <input type="checkbox" value='{ "folder" : "{$folder}", "file" : "{$files[item].text}" }' name="action_chkbx[]" />
                </div>
                <div class="col-xs-1">
                    <i class="{$files[item].icon} fa-2x"></i>
                </div>
                <div class="col-xs-10 condensed file_informations">
                    <a href="javascript:void(0);" data-filename="{$files[item].a_attr}" data-trigger="focus" data-toggle="popover" data-poload="index.php?page=executeGetFileDetails" class="{$files[item].type}" data-placement="right" data-delay="400" data-original-title="{$files[item].text}">{$files[item].text|truncate:35:"..."}</a><br />
                    <i>{$files[item].size}</i>
                </div>

            </div>

        {/section}    
    </form>


    <form id="uploadFiles" action="index.php?page=executeUploadFile" class="dropzone" enctype="multipart/form-data">
        <div class="fallback">
            <input name="file" type="file" multiple />
        </div>
    </form>
    <div style="position:absolute;bottom:60px;">{$folder}</div>
</div>

<script>

    $('[data-toggle="tooltip"]').tooltip();

    $('*[data-poload]').click(function () {
        var e=$(this);
        e.off('hover');
        $.get(e.attr('data-poload'), {
            filename : e.attr("data-filename")
        },function(d) {
        e.popover({ content: d , html : true , viewport : 'body' , delay : '0' }).popover('show');
        });
    });

    var myDropzone = new Dropzone("div#dropzone", {
        url: "index.php?page=executeUploadFile&folder={$folder}",
        clickable: false
    });

    myDropzone.on("complete", function (file) {
        $.ajax({
            url: "index.php",
            data: {
                page: "executeGetFileContent",
                filename: "{$folder}"
            },
            success: function (responseString) {

                //$('.file-tree').open_node(node);
                $(".file-content").html(responseString);
                $(".file-content").attr("data-folder", "{$folder}");
                //console.log(responseString);
            }
        });
    });

</script>