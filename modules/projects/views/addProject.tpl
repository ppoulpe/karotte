<form action='index.php?page=executeAddProject&token={$token.token_object}' method='post' id='addProjectForm' name='addProjectForm' class='form-horizontal' role="form" enctype="multipart/form-data">
    <div class="form-group">
        <label class="col-sm-2 control-label">Nom du projet</label>
        <div class="col-sm-3">
            <input type="text" title="Nom du projet" data-placement="bottom" data-toggle="tooltip" placeholder="Nom du projet" class="form-control" data-original-title="Nom du projet" id='name' name='name' required="required">
        </div>                 
    </div>    
    <div class="form-group">
        
        <label class="col-sm-2 control-label" for="picture">Logo du projet</label>
        <div class="col-sm-6 col-md-6 col-lg-6">
            <!-- <input type="hidden" name="MAX_FILE_SIZE" value="12345" /> -->
        <input type="file" id="picture" name="picture">
        <p class="help-block">Example block-level help text here.</p>
        </div>
    </div>   
    <div class="form-group">
        <div class="col-sm-6 col-sm-offset-2 col-md-6 col-md-offset-2 col-lg-6 col-lg-offset-2">
            <textarea class="form-control wysihtml5" id='description' name='description' rows="5"></textarea>
        </div>                 
    </div>                
    <div class="form-group">

        <div class="col-sm-2 col-sm-offset-2">
            <a href="index.php" class="btn btn-warning">Annuler</a>
            <input type='submit' value='Sauvegarder' class='btn btn-success'/>
        </div>
    </div>                   
</form>