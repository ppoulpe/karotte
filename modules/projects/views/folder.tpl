<div class="col-sm-3 full-height file-tree-container no-padding">
    <div class="col-sm-12 file-tree" id="{$project.project_id}"></div>
</div>
<div class="col-sm-9 full-height file-content-container no-padding">
    <div class="col-sm-12" style="padding:10px;position:absolute;top:0;border-bottom:1px solid hsla(0,0%,80%,1);width:78vw;">
        <a href="javascript:void(0);" class="modalDialog" data-ajax="index.php?page=getDialog&dialog=addFolder&pid={$project.project_id}"><i class="fa fa-plus"></i> Ajouter un dossier</a>
        / <a href="javascript:void(0);" class="download"><i class="fa fa-compress"></i> Télécharger les fichiers</a>
    </div>    
    <div class="col-sm-12 file-content" data-folder="{$base_url}/documents/projets/project_{$project.project_id}/ressources"></div>
</div>