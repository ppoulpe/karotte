<div>
    <span class="sub-sidebar-title">Projets</span>
    <ul class="projects-menu">
        {section name=item loop=$projects}
            
            <li class="btn-group">
                <a class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                     {$projects[item].name} <span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                    <li><a href="javascript:void(0);" class="modalDialog" data-ajax="{$live_url}/modules/projects/index.php?page=getDialog&pid={$projects[item].project_id}&dialog=editProject"><i class="fa fa-pencil"></i> Editer</a></li>
                    <li><a href="{$live_url}/modules/projects/index.php?page=todolist&pid={$projects[item].project_id}"><i class="fa fa-list"></i> A faire</a></li>
                    <li><a href="{$live_url}/modules/projects/index.php?page=editProject&pid={$projects[item].project_id}&view=folder"><i class="fa fa-folder"></i> Fichiers</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="{$live_url}/modules/projects/index.php?page=deleteProject&pid={$projects[item].project_id}"><i class="fa fa-trash"></i> Archiver</a></li>
                </ul>
            </li>

        {/section}
    </ul>
    <div class="sub-sidebar-button-bottom">
        <span class="fa-stack fa-lg">
            <i class="fa fa-circle fa-stack-2x"></i>
            <a href="javascript:void(0);" class="modalDialog" data-ajax="index.php?page=getDialog&dialog=addProject"><i class="fa fa-plus fa-stack-1x"></i></a>
          </span>
        
    </div>
</div>
