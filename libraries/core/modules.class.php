<?php

class SFModules {

    // Déclaration des librairies externes (helpers)
    protected $userHelper = null;
    protected $sfUsersHelper = null;
    protected $logHelper = null;
    protected $viewHelper = null;
    protected $controllerHelper = null;
    protected $breadcrumb = null;

    function __construct() {

        $this->userHelper = new User();
        $this->viewHelper = new view();
        $this->controllerHelper = new Controller();
        $this->breadcrumb = new Breadcrumb();
    }

    /**
     * @desc Verifie la légitimité d'un utilisateur
     * @return <boolean>
     */
    public function verifyConnexion() {

        if ($this->userHelper->isSigned() && $this->userHelper->isAdmin()) {
            return true;
        } else {

            $this->userHelper->logout();
            return false;
        }
    }

    /**
     * @desc Charge une fonction.
     * @param <array> Paramètres de l'action a faire. Contient égualement le nom de la fonction a executer.
     * @param <void>
     */
    public function load($params = array()) {

        // Si je ne suis pas un administrateur ou que je ne suis pas connecté
        if (!$this->userHelper->isSigned() || !$this->userHelper->isAdmin()) {

            // Si l'action qui a été lancé est une connexion, je laisse le script courir, sinon, retour à la connexion.
            if ($params["page"] != "login" && $params["page"] != "executeLogin") {

                $this->userHelper->logout();
                $this->viewHelper->_goTo(LIVE_URL . "/modules/users/index.php?page=login");
                //$this->viewHelper->_goTo("index.php?page=login");

                return false;
            }
        }

        // On teste la présence d'une valeur dans le paramètre PAGE.
        // Si pas de valeur on charge la vue par dafaut du module (index)
        if (isset($params["page"]) && $params["page"] != "") {

            $function = $params["page"];
            unset($params["page"]); // On détruit la valeur de l'action, pour éviter de charger une fonction au lieu d'une procédure
        } else {

            $function = "index";
        }

        if (method_exists($this, $function)) {

            if (!empty($params)) {
                $this->$function($params);
            } else
                $this->$function();

            return true;
        }else {
            
            die("Tentative de piratage");
            //$this->index(array("alert" => "Cette fonction n'existe pas !"));
            return false;
        }

        return false;
    }

}
