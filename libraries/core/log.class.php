<?php

/**
 *
 * Activity Plugin
 *
 * @author Jérôme Dumas
 *
 * @copyright Copyright 2013, Jérôme Dumas
 * @version 0.1
 *
 */

class Log {

    private static $instance;
    private $pdo = null;

    private function __construct() {
        
        $db = Database::getInstance();
        $this->pdo = $db->getDbh();    
        
    }  
    
    /**
     * Regarde si un objet connexion a déjà été instancier,
     * si c'est le cas alors il retourne l'objet déjà existant
     * sinon il en créer un autre.
     * @return $instance
     */
    public static function getInstance()
    {
        if (!self::$instance instanceof self)
        {
            self::$instance = new self;
        }
        return self::$instance;
    }  
    
    public function areYouOk(){
        echo "Hi, i'm Activty plugin and i'm ok!";
    }

    /**
     * @desc Inscrit dans la table log une action utilisateur
     * @param <array> Tableau de l'action à logger. [Utilisateur, action, module, message]
     * @return <PDOStatement> résultat de la requete
     */
    public function insert($action = array()) {

        $stmt = $this->pdo->prepare("INSERT INTO log (user_id, action, module, title, message, statut) VALUES (:user, :action, :module, :title, :message, 1);");

        $stmt->bindParam(':user', $_SESSION["userData"]["data"]["ID"]);
        $stmt->bindParam(':action', $this->actions[$action["action"]]);
        $stmt->bindParam(':module', $action["module"]);
        $stmt->bindParam(':title', $action["title"]);
        $stmt->bindParam(':message', $action["message"]);

        try {

            return $stmt->execute();
        } catch (PDOException $exc) {

            return $exc;
            
        }
        
        return false;
    }

    /**
     * @desc Récupère l'ensemble de l'activité d'un membre du site.
     * @param <integer> ID de l'utilisateur
     * @return <array> Tableau de résultat avec l'activité d'un membre
     */
    public function getLogs($userId = null , $page = 1 , $activityToShow = "addContent") {
        
        // Nombre d'infos par page
        $pagination = 5;
        // Numéro du 1er enregistrement à lire
        $limit_start = ($page - 1) * $pagination;
        if($activityToShow != "all") $whereAndClause = "AND action = :type ";

        $sql = "SELECT log_id, title, action, module, message, time, user_id "
                . "FROM log "
                . "WHERE user_id = :user_id "
                . $whereAndClause
                . "AND statut = 1 "
                . "ORDER BY time DESC "
                . "LIMIT ".$limit_start.",".$pagination." ;";

        $stmt = $this->pdo->prepare($sql);

        $stmt->bindParam(':user_id', $userId);
        
        if($activityToShow != "all") $stmt->bindParam(':type', $activityToShow);

        try {
            $stmt->execute();
        } catch (PDOException $exc) {
            return $exc;
        }

        return $stmt->fetchAll();
    }

    public function delete($userId = null, $logId = null) {

        if ($userId == null || $logId == null) {
            return false;
        }

        $sql = "UPDATE log "
                . "SET statut = 0 "
                . "WHERE log_id = :log_id "
                . "AND user_id = :user_id ";

        $stmt = $this->pdo->prepare($sql);

        $stmt->bindParam(':user_id', $userId);
        $stmt->bindParam(':log_id', $logId);

        try {
            $stmt->execute();
        } catch (PDOException $exc) {
            return $exc;
        }

        return $stmt->rowCount();
    }

}
