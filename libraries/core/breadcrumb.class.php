<?php

/*
 * Copyright (C) 2015 Jérôme Dumas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

class Breadcrumb {

    var $output;
    var $crumbs = array();
    var $location;

    /*
     * Constructor
     */

    function Breadcrumb() {

        if ($_SESSION['breadcrumb'] != null) {
            $this->crumbs = $_SESSION['breadcrumb'];
        }
    }

    /*
     * Add a crumb to the trail:
     * @param $label - The string to display
     * @param $url - The url underlying the label
     * @param $level - The level of this link.  
     *
     */

    function add($label, $url, $level) {

        $crumb = array();
        $crumb['label'] = $label;
        $crumb['url'] = $url;

        if ($crumb['label'] != null && $crumb['url'] != null && isset($level)) {

            while (count($this->crumbs) > $level) {
                array_pop($this->crumbs); //prune until we reach the $level we've allocated to this page
            }

            if (!isset($this->crumbs[0]) && $level > 0) { //If there's no session data yet, assume a homepage link
                $this->crumbs[0]['url'] = "/index.php";
                $this->crumbs[0]['label'] = "Home";
            }

            $this->crumbs[$level] = $crumb;
        }

        $_SESSION['breadcrumb'] = $this->crumbs; //Persist the data
        //$this->crumbs[$level]['url'] = null; //Ditch the underlying url for the current page.
    }
    
    function clear(){
        unset($_SESSION['breadcrumb']);
    }

    /*
     * Output a semantic list of links.  See above for sample CSS.  Modify this to suit your design.
     */

    function output() {
        return $_SESSION['breadcrumb'];
    }

}