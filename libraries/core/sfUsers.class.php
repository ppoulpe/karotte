<?php

require_once("uFlex/autoload.php");

class User extends ptejada\uFlex\User {

    public function __construct() {
        
        parent::__construct();

        //Add database credentials
        $this->config->database->host = DBHOST;
        $this->config->database->user = DBUSER;
        $this->config->database->password = DBPASSWD;
        $this->config->database->name = DBNAME;
        
        $this->start();
    }
    
    /**
     * @desc Permet de savoir si un utilisateur est un administrateur
     * @return <boolean>
     */
    function isAdmin(){
        
        if($this->getProperty("GroupID") == 2){
            return true;
        }else{
            //var_dump($this->getProperty("Username"));
            return false;
        }
    }

}
