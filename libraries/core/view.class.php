<?php

/**
 * Description of view
 *
 * @author Jérôme DUMAS
 */
class View {
    
    private $allowed_modules = array(
        "projects",
        "users",
        "parameters",
        "contacts"
    );

    public function __construct() {
        
        $this->smarty = new Smarty();

        $this->smarty->template_dir = BASE_URL . '/templates/';
        $this->smarty->compile_dir = BASE_URL . '/templates_c/';
        $this->smarty->config_dir = BASE_URL . '/configs/';
        $this->smarty->cache_dir = BASE_URL . '/cache/';

        // Chargement des menus
        foreach ($this->allowed_modules as $value) {
            if (is_dir(BASE_URL . "/modules/" . $value)) {

                // On charge les fichiers de config de chaque module pour récupérer les menus
                include BASE_URL . "/modules/" . $value . "/config.php";

                $menus[$value] = (isset($config[$value]["menu"])) ? $config[$value]["menu"] : "";
                $menus[$value]["icon"] = (isset($config[$value]["icon"])) ? $config[$value]["icon"] : "";
                $menus[$value]["target"] = (isset($config[$value]["target"])) ? $config[$value]["target"] : "ajax";
                
                $actions[$value]["actions"] = (isset($config[$value]["actions"])) ? $config[$value]["actions"] : "";
            }
        }

        // Chargement des ressources nécaissaires pour chaque page.
        $this->smarty->assign(
                array(
                    "live_url" => LIVE_URL,
                    "site_name" => SITENAME,
                    "base_url" => BASE_URL,
                    "referer" => $_SERVER['HTTP_REFERER'],
                    "main_menu" => $menus,
                    "actions" => $actions
                )
        );
    }

    public function areYouOk() {
        return "Hi, i'm view core and i'm ok!";
    }

    public function setGlobalAssign($assign) {
        $this->smarty->assign($assign);
        return true;
    }

    public function getSmartyObject() {
        return $this->smarty;
    }

    /**
     * @desc Charge une vue dans le layout.
     * @param <string> Chemin vers la vue à charger dans le layout. Le chemin peut $etre définit dans le fichier <config.php> via la variable d'env. <VIEW_PATH>
     * @param <string> Module ou se trouve la vue
     * @param <array> Tableau de <string> contenant les valeurs smarty à transmettre à la vue
     * @param <boolean> On renseigne la nature de l'affiche. Si il doit être sécurisé, on vérifie que l'utilisateur et un admin, et on génére un token
     * @return <boolean> Retourne false si aucune vue ou aucun module n'est passé en paramètre. Charge ensuite la vue
     */
    public function renderIntoLayout($view = null, $smarty_assign = array(), $layout = "layout.tpl") {

        // Si le mode secure est activé et que la session est vide, on arrête tout
        if ($view === NULL || $layout === NULL) {
            //die("Forbidden");
            return false;
        } else {

            $this->smarty->assign($smarty_assign);
            $fetch = $this->smarty->fetch($view);

            // /!\ Si j'ai des variable dans le global assign, il ne faut pas les écraser avec un tableau vide (notamment les JS et les CSS)
            // Il faudra par contre se retaper les inclusions si j'ai un cas spécifique.
            if (!empty($smarty_assign["javascript"])) {
                $this->smarty->assign(array("javascript" => $smarty_assign["javascript"]));
            }
            
            if (!empty($smarty_assign["css"])) {
                $this->smarty->assign(array("css" => $smarty_assign["css"]));
            }            

            // Assignation des variables obligatoires smarty
            $this->smarty->assign(array(
                "content" => $fetch,
                "session" => $_SESSION,
                "alert" => (isset($smarty_assign["alert"])) ? $smarty_assign["alert"] : "",
                "breadcrumb" => $_SESSION["breadcrumb"],
                "is_home" => $this->isHome()
            ));

            $this->smarty->display(LAYOUT_PATH . $layout);
            
            return true;
        }

        return false;
    }

    /**
     * @desc Permet de récupérer le code HTML d'une vue, afin de faire des imbrications de vues.
     * @param <string> Nom de la vue à charger dans le layout
     * @param <string> Module ou se trouve la vue
     * @param <array> Tableau de <string> contenant les valeurs smarty à transmettre à la vue
     * @param <array> de <string> Contenant les renseignement du Token
     * @param <string> Environnement. Front ou back
     * @return <boolean> or <html> Retourne false si aucune vue ou aucun module renseigné. Sinon, retourne le HTML de la vue.
     */
    public function fetch($view = null, $smarty_assign = array(), $token = array()) {

        if ($view === NULL) {
            return false;
        }

        $smarty_assign["session"] = $_SESSION;

        if (!empty($token)) {
            $smarty_assign[$token["token_name"] . "_token"] = $token["token_object"];
        }

        $this->smarty->assign($smarty_assign);

        return $this->smarty->fetch($view);
    }

    /**
     * @desc Libère des assigns Smarty.
     * @param <array> de <string> Tableau contenant les variables à libérer.
     */
    public function _unset($vars = array()) {
        $this->smarty->clearAssign($vars);
    }

    /**
     * @desc Redirige vers la page d'accueil et affiche un message si besoin.
     * @param <string> Message a afficher sur la page d'accueil
     */
    public function _goToIndexPage($message = null) {

        //header('location:' . LIVE_URL);
        
        echo '<script type="text/javascript"> location.href = "'.LIVE_URL.'"; </script>';
        
    }

    public function _goTo($url = null) {
        
        //header('location:' . $url);
        
        echo '<script type="text/javascript"> location.href = "'.$url.'"; </script>';
    }

    /**
     * @desc Redirige vers la page précédent la dernière requête.
     */
    public function _goToPreviousPage() {

        //header('location:' . $_SERVER["HTTP_REFERER"]);
        
        echo '<script type="text/javascript"> location.href = "'.$_SERVER["HTTP_REFERER"].'"; </script>';
    }

    /**
     * @desc Permet de savoir si on est sur la page d'accueil.
     * @return <boolean> True si on est sur l'accueil. False sinon.
     */
    public function isHome() {

        if (HOME_URL == "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']) {
            return true;
        } else {
            return false;
        }
    }

}
