<?php

session_start();

require_once ('view.class.php');
require_once ('database.class.php');

/**
 * Application class file
 * @author Jérôme Dumas
 * @package MSFramework
 * Class Application
 */
class Controller {

    /**
     * @desc Objet smarty pour charger la vue des modules
     * @var type object
     */
    protected $pdo = null;

    public function __construct() {

        $db = Database::getInstance();
        $this->pdo = $db->getDbh();
        //$this->rewrite_url = $url->createUrl("index.php?action=profile");
    }

    /**
     * @desc Permet de savoir si la classe est bien formée et opérationnelle
     * @return <string> Message type
     */
    public function areYouOk() {
        return "Hi, i'm controller core and i'm ok!";
    }

    /**
     * @desc Génère un Token pour éviter les vol de compte.
     * @desc Voir http://fr.openclassrooms.com/informatique/cours/securisation-des-failles-csrf
     * @param <string> Nom du token
     * @return <string> Token.
     */
    function _setToken($name = "") {
        $token = uniqid(rand(), true);

        $_SESSION[$name . '_token'] = $token;
        $_SESSION[$name . '_token_time'] = time();

        $objToken["token_name"] = $name;
        $objToken["token_object"] = $token;
        return $objToken;
    }

    /**
     * @desc Récupère l'adresse IP du visiteur. Utile pour enregistrer des logs de connexion lors de la connexion à l'administration
     * @return <string> Adresse IP
     */
    public function _getVisitorIp() {
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } elseif (isset($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

}
