<?php

/*
 * Copyright (C) 2015 Jérôme Dumas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

session_start();

require_once ('database.class.php'); 

/**
 * Description of configuration
 *
 * @author jdumas
 */
class configuration {

    private static $instance;
    //private $pdo;
    private $parameters = array();
    
    public function __construct() {
        
        $db = Database::getInstance();
        $this->pdo = $db->getDbh();
        
    }

    /**
     * Regarde si un objet connexion a déjà été instancier,
     * si c'est le cas alors il retourne l'objet déjà existant
     * sinon il en créer un autre.
     * @return $instance
     */
    public static function getInstance(){
        if (!self::$instance instanceof self)
        {
            self::$instance = new self;
        }
        return self::$instance;
    }
    
    /**
     * @desc Permet de savoir si la classe est bien formée et opérationnelle
     * @return <string> Message type
     */
    public function areYouOk() {
        return "Hi, i'm configuration core and i'm ok!";
    }    
    
    public function setConfiguration($module = null, $config = array()){
        
        if($module){
            $this->parameters[$module] = $config;
        }else{
            return false;
            //$this->parameters = $config;
        }
        
    }
    
    public function setValue($key = null, $value = null){
        $this->parameters[$key] = $value;
    }
    
    public function getConfiguration(){
        return $this->parameters;
    }
    
    public function getValue($parameterName){
        
        if(isset($this->parameters[$parameterName]))
            return $this->parameters[$parameterName];
        
        return false;
        
    }

}
