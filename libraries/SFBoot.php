<?php

/*******************************************************************************
 * @author     Jérôme Dumas
 * @copyright  2014 Jérôme Dumas
 * @license    http://www.php.net/license/3_0.txt  PHP License 3.0
 * @version    0.1
 ******************************************************************************/

require_once ('core/configuration.class.php');
require_once '../../config.php';

/*******************************************************************************
 * CHARGEMENT DU COEUR DU FRAMEWORK
 * 1/ SMARTY. OBLIGATOIRE DE LE PLACER AVANT view.class.php. Moteur de template
 * 2/ Class view, permet de charger des vues via smarty.
 * 2/ Class Controller, quelques fonctions utiles, comme les tokens
 * 3/ Class Database, contient la connexion à la base. 
 ******************************************************************************/


require_once ('core/smarty/Smarty.class.php');
//require_once ('core/PFBC/Form.php');

require_once ('core/view.class.php');
require_once ('core/controller.class.php');
require_once ('core/database.class.php');

require_once ('core/breadcrumb.class.php');
require_once ('core/sfUsers.class.php');
require_once ('core/modules.class.php');

require_once ('core/log.class.php');



/*******************************************************************************
 * DEBUGER
 ******************************************************************************/

if(DEBUG){
    ini_set('display_errors', 1);
    error_reporting(E_ALL);     
}else{
    ini_set('display_errors', 0);
}
    
/*******************************************************************************
 * CHARGEMENT DES PLUGINS
 * 1/ Class uflex, Gestion des utilisateurs.
 * 2/ Class Activity Gestion de l'activité des membres.
 * 3/ Class PHPMailer, class d'envoi de mails. 
 * 4/ Class json permettant le traitement de fichier json
 ******************************************************************************/
require_once ('helpers/files.helper.php');
//require_once ('plugins/users.plugin.php');
//require_once ('plugins/activity.plugin.php');
//require_once ('plugins/json.plugin.php');
//require_once ('plugins/form.plugin.php');
