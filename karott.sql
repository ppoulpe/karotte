-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Ven 06 Novembre 2015 à 15:12
-- Version du serveur :  5.6.17
-- Version de PHP :  5.5.12

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `karott`
--

-- --------------------------------------------------------

--
-- Structure de la table `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
  `group_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `groups`
--

INSERT INTO `groups` (`group_id`, `name`) VALUES
(1, 'Enregistré'),
(2, 'Administrateur');

-- --------------------------------------------------------

--
-- Structure de la table `log`
--

CREATE TABLE IF NOT EXISTS `log` (
  `log_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `action` varchar(255) DEFAULT NULL,
  `module` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `message` mediumtext,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `statut` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=237 ;

--
-- Contenu de la table `log`
--

INSERT INTO `log` (`log_id`, `user_id`, `action`, `module`, `title`, `message`, `time`, `statut`) VALUES
(192, 1, NULL, 'projects', 'Ajout d''un nouveau projet', 'Une erreur est survenue pendant l''ajout d''un nouveau projet', '2015-11-02 15:40:20', 1),
(193, 1, NULL, 'projects', 'Ajout d''un nouveau projet', 'Une erreur est survenue pendant l''ajout d''un nouveau projet', '2015-11-02 15:41:04', 1),
(194, 1, NULL, 'projects', 'Ajout d''un nouveau projet', 'Une erreur est survenue pendant l''ajout d''un nouveau projet', '2015-11-02 15:51:50', 1),
(195, 1, NULL, 'projects', 'Ajout d''un nouveau projet', 'Une erreur est survenue pendant l''ajout d''un nouveau projet', '2015-11-02 15:58:40', 1),
(196, 1, NULL, 'projects', 'Ajout d''un nouveau projet', 'Une erreur est survenue pendant l''ajout d''un nouveau projet', '2015-11-02 15:59:34', 1),
(197, 1, NULL, 'projects', 'Ajout d''un nouveau projet', 'Un nouveau projet a été ajouté avec succès', '2015-11-02 16:00:42', 1),
(198, 1, NULL, 'projects', 'Ajout d''un nouveau projet', 'Un nouveau projet a été ajouté avec succès', '2015-11-02 19:49:46', 1),
(199, 1, NULL, 'projects', 'Ajout d''un nouveau projet', 'Un nouveau projet a été ajouté avec succès', '2015-11-02 20:37:34', 1),
(200, 1, NULL, 'projects', 'Ajout d''un nouveau projet', 'Un nouveau projet a été ajouté avec succès', '2015-11-02 20:48:15', 1),
(201, 1, NULL, 'projects', 'Mise à jour du projet #5', 'Le projet #5a été mis à jour !', '2015-11-03 18:18:54', 1),
(202, 1, NULL, 'projects', 'Mise à jour du projet #5', 'Le projet #5a été mis à jour !', '2015-11-03 18:18:57', 1),
(203, 1, NULL, 'projects', 'Mise à jour du projet #5', 'Le projet #5a été mis à jour !', '2015-11-03 18:26:44', 1),
(204, 1, NULL, 'projects', 'Mise à jour du projet #5', 'Le projet #5a été mis à jour !', '2015-11-03 18:27:03', 1),
(205, 1, NULL, 'projects', 'Mise à jour du projet #5', 'Le projet #5a été mis à jour !', '2015-11-03 18:30:57', 1),
(206, 1, NULL, 'projects', 'Mise à jour du projet #5', 'Le projet #5a été mis à jour !', '2015-11-03 18:31:09', 1),
(207, 1, NULL, 'projects', 'Mise à jour du projet #5', 'Le projet #5a été mis à jour !', '2015-11-03 18:31:28', 1),
(208, 1, NULL, 'projects', 'Mise à jour du projet #5', 'Le projet #5a été mis à jour !', '2015-11-03 18:31:38', 1),
(209, 1, NULL, 'projects', 'Mise à jour du projet #5', 'Une erreur est survenue pendant la mise à jour du projet #5', '2015-11-03 18:31:39', 1),
(210, 1, NULL, 'projects', 'Mise à jour du projet #5', 'Le projet #5a été mis à jour !', '2015-11-03 18:31:50', 1),
(211, 1, NULL, 'projects', 'Mise à jour du projet #5', 'Une erreur est survenue pendant la mise à jour du projet #5', '2015-11-03 18:31:51', 1),
(212, 1, NULL, 'projects', 'Mise à jour du projet #5', 'Le projet #5a été mis à jour !', '2015-11-03 18:32:14', 1),
(213, 1, NULL, 'projects', 'Mise à jour du projet #5', 'Une erreur est survenue pendant la mise à jour du projet #5', '2015-11-03 18:32:15', 1),
(214, 1, NULL, 'projects', 'Mise à jour du projet #5', 'Le projet #5a été mis à jour !', '2015-11-03 18:34:15', 1),
(215, 1, NULL, 'projects', 'Mise à jour du projet #5', 'Le projet #5a été mis à jour !', '2015-11-03 18:34:24', 1),
(216, 1, NULL, 'projects', 'Mise à jour du projet #5', 'Le projet #5a été mis à jour !', '2015-11-03 18:43:37', 1),
(217, 1, NULL, 'projects', 'Suppression du projet #', 'Le projet # a été Supprimé !', '2015-11-03 18:43:44', 1),
(218, 1, NULL, 'projects', 'Suppression du projet #', 'Le projet # a été Supprimé !', '2015-11-03 18:43:47', 1),
(219, 1, NULL, 'projects', 'Suppression du projet #', 'Le projet # a été Supprimé !', '2015-11-03 18:44:44', 1),
(220, 1, NULL, 'projects', 'Ajout d''un nouveau projet', 'Un nouveau projet a été ajouté avec succès', '2015-11-03 20:37:29', 1),
(221, 1, NULL, 'projects', 'Mise à jour du projet #6', 'Le projet #6a été mis à jour !', '2015-11-03 20:37:45', 1),
(222, 1, NULL, 'projects', 'Mise à jour du projet #6', 'Une erreur est survenue pendant la mise à jour du projet #6', '2015-11-03 20:42:50', 1),
(223, 1, NULL, 'projects', 'Mise à jour du projet #6', 'Une erreur est survenue pendant la mise à jour du projet #6', '2015-11-03 20:42:57', 1),
(224, 1, NULL, 'projects', 'Mise à jour du projet #6', 'Une erreur est survenue pendant la mise à jour du projet #6', '2015-11-03 20:43:06', 1),
(225, 1, NULL, 'projects', 'Mise à jour du projet #6', 'Une erreur est survenue pendant la mise à jour du projet #6', '2015-11-03 20:43:14', 1),
(226, 1, NULL, 'projects', 'Mise à jour du projet #6', 'Le projet #6a été mis à jour !', '2015-11-03 20:43:48', 1),
(227, 1, NULL, 'projects', 'Suppression du projet #', 'Le projet # a été Supprimé !', '2015-11-03 20:46:25', 1),
(228, 1, NULL, 'projects', 'Ajout d''un nouveau projet', 'Un nouveau projet a été ajouté avec succès', '2015-11-03 20:48:50', 1),
(229, 1, NULL, 'projects', 'Ajout d''un nouveau projet', 'Un nouveau projet a été ajouté avec succès', '2015-11-03 20:50:09', 1),
(230, 1, NULL, 'projects', 'Mise à jour du projet #8', 'Le projet #8a été mis à jour !', '2015-11-03 20:50:32', 1),
(231, 1, NULL, 'projects', 'Mise à jour du projet #8', 'Le projet #8a été mis à jour !', '2015-11-04 13:25:24', 1),
(232, 1, NULL, 'projects', 'Mise à jour du projet #2', 'Le projet #2a été mis à jour !', '2015-11-06 11:58:09', 1),
(233, 1, NULL, 'projects', 'Mise à jour du projet #3', 'Le projet #3a été mis à jour !', '2015-11-06 11:58:25', 1),
(234, 1, NULL, 'projects', 'Mise à jour du projet #4', 'Le projet #4a été mis à jour !', '2015-11-06 11:58:43', 1),
(235, 1, NULL, 'projects', 'Mise à jour du projet #7', 'Le projet #7a été mis à jour !', '2015-11-06 11:59:00', 1),
(236, 1, NULL, 'projects', 'Mise à jour du projet #8', 'Le projet #8a été mis à jour !', '2015-11-06 11:59:12', 1);

-- --------------------------------------------------------

--
-- Structure de la table `projects`
--

CREATE TABLE IF NOT EXISTS `projects` (
  `project_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  `description` mediumtext COLLATE utf8_bin,
  `picture` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `statut` tinyint(4) NOT NULL DEFAULT '1',
  `users_id` int(7) NOT NULL,
  PRIMARY KEY (`project_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=9 ;

--
-- Contenu de la table `projects`
--

INSERT INTO `projects` (`project_id`, `name`, `description`, `picture`, `statut`, `users_id`) VALUES
(2, 'AJR Médical', '', NULL, 1, 1),
(3, 'Frais de notaires', '', NULL, 1, 1),
(4, 'Adevimap', '', NULL, 1, 1),
(6, 'Mon premier projet', '<b>Description </b>de mon premier projet<br>', NULL, 0, 1),
(7, 'VDART', 'Cendrillon est-elle parmi nous ?<br>', NULL, 1, 1),
(8, 'Karotte', 'Cendrillon est-elle parmi nous ?<br>', 'Cendrillon_story.jpg', 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `ID` int(7) unsigned NOT NULL AUTO_INCREMENT,
  `Username` varchar(15) NOT NULL,
  `statut` varchar(140) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `surname` varchar(255) DEFAULT NULL,
  `birthday` varchar(10) DEFAULT NULL,
  `facebook_url` varchar(255) DEFAULT NULL,
  `twitter_url` varchar(255) DEFAULT NULL,
  `website_url` varchar(255) DEFAULT NULL,
  `Password` char(40) NOT NULL,
  `Email` varchar(100) NOT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  `Activated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Confirmation` char(40) NOT NULL,
  `RegDate` int(11) unsigned NOT NULL,
  `LastLogin` int(11) unsigned NOT NULL DEFAULT '0',
  `GroupID` tinyint(3) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT AUTO_INCREMENT=20 ;

--
-- Contenu de la table `users`
--

INSERT INTO `users` (`ID`, `Username`, `statut`, `name`, `surname`, `birthday`, `facebook_url`, `twitter_url`, `website_url`, `Password`, `Email`, `avatar`, `Activated`, `Confirmation`, `RegDate`, `LastLogin`, `GroupID`) VALUES
(1, 'jdumas', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean dictum nunc id sollicitudin porta. Etiam condimentum elementum erat sit', '', 'Jérôme', '14/12/1989', '', '', '', 'dae3fd5c802e1ba14b6321a4f6473d4f', 'hey@level2.fr', '549010_705346172815556_1836557104_n.jpg', 1, '', 1397584065, 1446799157, 2),
(2, 'jdumas2', NULL, NULL, NULL, NULL, '', '', '', 'e70038bc0097bb1ba9e94e5012c4ad80', 'valkiki83@msn.com', 'avatar_fofo.jpg', 1, '', 1397741352, 1397764638, 1),
(3, 'jdumas3', 'Yeah2!', NULL, NULL, NULL, NULL, NULL, NULL, '421e60721966bf7603644f01cc521489', 'valkiki83@msn3.com', '507346386_95265.gif', 1, '', 1403006444, 1405018316, 1),
(4, 'ddumas', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '94a3e388f0f9ac03cb20a7972f85d32f', 'ddumas@lol.fr', '4dc24a8e.jpg', 1, '', 1403547810, 1405021204, 1),
(5, 'ddumas2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '822bdd5e97b27de788a8793f0f0d1b90', 'ddumas2@lol.fr', 'avatar.jpg', 1, '', 1403549026, 1405420447, 1),
(6, 'ddumas3', 'Regis craint!', NULL, NULL, NULL, NULL, NULL, NULL, 'e87d581757919fbe09f7b4174f18f733', 'ddumas3@lol.fr', '328013.jpg', 1, '', 1403549039, 1404844131, 1),
(7, 'ddumas4', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '9880188b8e902cbfad8ee83bfbf341b8', 'ddumas4@lol.fr', 'avatar.jpg', 1, '', 1403549059, 1403549102, 1),
(8, 'schwarzy', 'Vous vous Ãªtes Ã©garÃ©s. Profitez-en pour lire un peu!', 'Schwarzenegger', 'Arnold', '30/07/1947', NULL, NULL, NULL, 'db8db8a97283ac79a73a65ef2eacbe0e', 'schwarzy@sweet-nerds.fr', 'terminator.jpg', 1, '', 1403868614, 1403879802, 3),
(9, 'noavatar', NULL, '', '', NULL, '', '', '', '457ea9a54ed45379cb363743cc6cc8db', 'noavatar@test.fr', 'avatar.jpg', 0, '', 1405420807, 1405420821, 1),
(13, 'jdumas777', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dae3fd5c802e1ba14b6321a4f6473d4f', 'jdumas@level7754.fr', 'avatar.jpg', 1, '', 1406199102, 0, 1),
(15, 'jdoe', NULL, 'DOE', 'John', NULL, 'johndoe', 'johndoe', 'johndoe.fr', 'dae3fd5c802e1ba14b6321a4f6473d4f', 'jdoe2@sweet-nerds.fr', 'avatar.jpg', 1, '', 1406200122, 0, 1),
(19, 'admin', NULL, '', '', NULL, '', '', '', 'e70038bc0097bb1ba9e94e5012c4ad80', 'adminbenjisoft@level2.fr', 'avatar.jpg', 1, '', 1406296277, 0, 2);
SET FOREIGN_KEY_CHECKS=1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
